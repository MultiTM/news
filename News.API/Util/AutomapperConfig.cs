﻿using AutoMapper;
using News.Core.DTO;
using News.Data.Entities;

namespace News.API.Util
{
    public class AutomapperConfig : Profile
    {
        public AutomapperConfig()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();

            CreateMap<RssSource, RssSourceDto>().ReverseMap();

            CreateMap<NewsItemDto, NewsItem>();
            CreateMap<NewsItem, NewsItemDto>()
                .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name));
                //.ForMember(dest => dest.RssSourceName, opt => opt.MapFrom(src => src.RssSource.Name));
            
            CreateMap<CommentDto, Comment>();
            CreateMap<Comment, CommentDto>()
                .ForMember(dest => dest.Author, opt => opt.MapFrom(src => src.User.Username))
                .ForMember(dest => dest.NewsItemTitle, opt => opt.MapFrom(src => src.NewsItem.Title));

            CreateMap<SavedNewsItemDto, SavedNewsItem>();
            CreateMap<SavedNewsItem, SavedNewsItemDto>()
                .ForMember(dest => dest.NewsItemTitle, opt => opt.MapFrom(src => src.NewsItem.Title));

            CreateMap<SubscribeDto, Subscribe>();
            CreateMap<Subscribe, SubscribeDto>()
                .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name));
        }
    }
}