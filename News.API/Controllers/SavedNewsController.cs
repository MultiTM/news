﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using News.Core.DTO;
using News.Core.Services;

namespace News.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SavedNewsController : Controller
    {
        private readonly SavedNewsService _savedNewsService;
        private readonly UserService _userService;

        public SavedNewsController(SavedNewsService savedNewsService, UserService userService)
        {
            _userService = userService;
            _savedNewsService = savedNewsService;
        }

        [HttpGet("/api/user/saved")]
        [ProducesResponseType(typeof(IEnumerable<SavedNewsItemDto>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetForUser()
        {
            var user = await _userService.GetByUsername(HttpContext.User.Identity.Name);
            if (user == null)
            {
                return NotFound("User not found");
            }

            var items = await _savedNewsService.GetSavedByUser(user.Id);

            return Ok(items);
        }

        [HttpGet("/api/user/saved/{newsItemId}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> IsInSaved(int newsItemId)
        {
            var user = await _userService.GetByUsername(HttpContext.User.Identity.Name);
            if (user == null)
            {
                return NotFound("User not found");
            }

            var saved = await _savedNewsService.GetSavedByUser(user.Id);

            return Ok(saved.Any(savedNewsItem => savedNewsItem.NewsItemId == newsItemId));
        }

        [HttpPost("/api/user/saved/{newsItemId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddToSaved(int newsItemId)
        {
            var user = await _userService.GetByUsername(HttpContext.User.Identity.Name);
            if (user == null)
            {
                return Unauthorized();
            }
            var item = new SavedNewsItemDto()
            {
                UserId = user.Id,
                NewsItemId = newsItemId
            };

            var success = await _savedNewsService.AddToSaved(item);
            if (!success)
            {
                return BadRequest("User or news is invalid");
            }

            return Ok();
        }

        [HttpDelete("/api/user/saved/{newsItemId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> RemoveFromSaved(int newsItemId)
        {
            var user = await _userService.GetByUsername(HttpContext.User.Identity.Name);
            var item = new SavedNewsItemDto { UserId = user.Id, NewsItemId = newsItemId};

            var success = await _savedNewsService.RemoveFromSaved(item);
            if (!success)
            {
                return BadRequest("User or news is invalid");
            }

            return Ok();
        }
    }
}