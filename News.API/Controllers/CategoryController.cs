﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using News.Core.DTO;
using News.Core.Services;
using News.Core.Util;

namespace News.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly CategoryService _categoryService;

        public CategoryController(CategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        // GET api/values
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(typeof(IEnumerable<CategoryDto>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            var items = await _categoryService.GetAllAsync();
            if (items == null)
            {
                return NotFound();
            }

            return Ok(items);
        }
        
        // GET api/values/5
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CategoryDto), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var item = await _categoryService.GetItem(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // POST api/values
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody]CategoryDto item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            await _categoryService.Add(item);

            return Ok();
        }

        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> Put([FromBody] CategoryDto item)
        {
            if (item == null || await _categoryService.GetItem(item.Id) == null)
            {
                return BadRequest();
            }

            await _categoryService.Update(item);

            return Ok();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _categoryService.GetItem(id);
            if (item == null)
            {
                return BadRequest();
            }

            await _categoryService.Remove(item);

            return Ok();
        }
    }
}