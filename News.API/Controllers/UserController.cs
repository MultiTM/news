﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using News.Core.DTO;
using News.Core.Services;
using News.Core.Util;

namespace News.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            var users = await _userService.GetAllAsync();
            foreach (var user in users)
            {
                user.Password = null;
            }

            return Ok(users);
        }

        [HttpGet("/api/[controller]/getPage")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(IEnumerable<UserDto>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetPage([FromQuery]PageInfo pageInfo)
        {
            var items = await _userService.GetPage(pageInfo);
            var pageCount = await _userService.GetPageCount(pageInfo);
            if (items == null)
            {
                return NotFound();
            }

            var userDtos = items.ToList();
            foreach (var user in userDtos)
            {
                user.Password = null;
            }

            var pageMetadata = new {pageInfo, pageCount, data = userDtos };

            return Ok(pageMetadata);
        }

        [HttpPost]
        [Route("/api/user/login")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetToken([FromBody]UserDto userCreds)
        {
            var user = await _userService.GetByUsername(userCreds.Username);
            if (user == null)
            {
                return BadRequest();
            }

            var encodedToken = await _userService.LogIn(userCreds);
            if (string.IsNullOrEmpty(encodedToken))
            {
                return BadRequest("Invalid login or password");
            }

            var userData = new {token = encodedToken, username = user.Username, userId = user.Id};
            return Ok(userData);
        }

        [HttpPost]
        [Route("/api/user/register")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Register([FromBody]UserDto user)
        {
            if (user == null)
            {
                return BadRequest("Error in request data. Please, check it and try again");
            }

            if (await _userService.Register(user))
            {
                return Ok();
            }
            else
            {
                return BadRequest("Error in registration. Please, try again later.");
            }
        }
    }
}