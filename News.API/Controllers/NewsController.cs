﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using News.Core.DTO;
using News.Core.Services;
using News.Core.Util;

namespace News.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class NewsController : Controller
    {
        private readonly NewsService _newsService;

        public NewsController(NewsService newsService)
        {
            _newsService = newsService;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(NewsItemDto), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var item = await _newsService.GetItem(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpGet("/api/[controller]/getPage")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(IEnumerable<NewsItemDto>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetPage([FromQuery]PageInfo pageInfo, [FromQuery]int? userId = null)
        {
            var items = await _newsService.GetPage(pageInfo, userId);
            var pageCount = await _newsService.GetPageCount(pageInfo, userId);
            if (items == null)
            {
                return NotFound();
            }

            var pageMetadata = new { pageInfo, pageCount, data = items};

            return Ok(pageMetadata);
        }

        // POST api/values
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> Post([FromBody] NewsItemDto item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            await _newsService.Add(item);

            return Ok();
        }
        
        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> Put([FromBody] NewsItemDto item)
        {
            if (item == null || await _newsService.GetItem(item.Id) == null)
            {
                return BadRequest();
            }

            await _newsService.Update(item);

            return Ok();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _newsService.GetItem(id);
            if (item == null)
            {
                return BadRequest();
            }

            await _newsService.Remove(item);

            return Ok();
        }
    }
}