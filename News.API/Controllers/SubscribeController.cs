﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using News.Core.DTO;
using News.Core.Services;

namespace News.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubscribeController : Controller
    {
        private readonly SubscribeService _subscribeService;
        private readonly UserService _userService;

        public SubscribeController(SubscribeService subscribeService, UserService userService)
        {
            _subscribeService = subscribeService;
            _userService = userService;
        }

        [HttpGet("/api/user/subscribes")]
        [ProducesResponseType(typeof(IEnumerable<SubscribeDto>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetForUser()
        {
            var user = await _userService.GetByUsername(HttpContext.User.Identity.Name);
            if (user == null)
            {
                return NotFound("User not found");
            }

            var items = await _subscribeService.GetUserSubscribes(user.Id);

            return Ok(items);
        }

        [HttpGet("/api/user/subscribes/{categoryId}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> IsInSubscribes(int categoryId)
        {
            var user = HttpContext.User.FindFirstValue("Id");
            if (string.IsNullOrEmpty(user))
            {
                return NotFound("User not found");
            }

            var subs = await _subscribeService.GetUserSubscribes(Int32.Parse(user));

            return Ok(subs.Any(subItem => subItem.CategoryId == categoryId));
        }

        [HttpPost("/api/user/subscribes/{categoryId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Subscribe(int categoryId)
        {
            var user = await _userService.GetByUsername(HttpContext.User.Identity.Name);
            var item = new SubscribeDto()
            {
                UserId = user.Id,
                CategoryId = categoryId
            };

            var success = await _subscribeService.Subscribe(item);
            if (!success)
            {
                return BadRequest("User or category is invalid");
            }

            return Ok();
        }

        [HttpDelete("/api/user/subscribes/{categoryId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Unsubscribe(int categoryId)
        {
            var user = await _userService.GetByUsername(HttpContext.User.Identity.Name);
            var item = new SubscribeDto()
            {
                UserId = user.Id,
                CategoryId = categoryId
            };

            var success = await _subscribeService.Unsubscribe(item);
            if (!success)
            {
                return BadRequest("User or category is invalid");
            }

            return Ok();
        }
    }
}