﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using News.Core.DTO;
using News.Core.Services;

namespace News.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : Controller
    {
        private readonly CommentService _commentService;
        private readonly UserService _userService;
        private readonly NewsService _newsService;

        public CommentController(CommentService commentService, UserService userService, NewsService newsService)
        {
            _commentService = commentService;
            _userService = userService;
            _newsService = newsService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CommentDto>),200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            var items = await _commentService.GetAllAsync();
            if (items == null)
            {
                return NotFound();
            }

            return Ok(items);
        }

        [HttpGet("/api/user/comments")]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<CommentDto>),200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetByUser()
        {
            var user = await _userService.GetByUsername(HttpContext.User.Identity.Name);
            if (user == null)
            {
                return NotFound("User not found");
            }

            var items = await _commentService.GetCommentsByUserId(user.Id);

            return Ok(items);
        }

        [HttpGet("/api/news/{newsItemId}/comments")]
        [ProducesResponseType(typeof(IEnumerable<CommentDto>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetByNews(int newsItemId)
        {
            var newsItem = await _newsService.GetItem(newsItemId);
            if (newsItem == null)
            {
                return NotFound("NewsItem not found");
            }

            var items = await _commentService.GetCommentsByNewsItemId(newsItemId);

            return Ok(items);
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddComment(CommentDto comment)
        {
            var user = await _userService.GetByUsername(HttpContext.User.Identity.Name);
            comment.UserId = user.Id;

            var success = await _commentService.AddComment(comment);
            if (!success)
            {
                return BadRequest("User or news is invalid");
            }

            return Ok();
        }
    }
}