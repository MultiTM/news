﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using News.Core.DTO;
using News.Core.Services;
using News.Core.Util;

namespace News.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class RssSourceController : Controller
    {
        private readonly RssSourceService _rssSourceService;

        public RssSourceController(RssSourceService rssSourceService)
        {
            _rssSourceService = rssSourceService;
        }

        // GET api/values
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<RssSourceDto>), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            var items = await _rssSourceService.GetAllAsync();
            if (items == null)
            {
                return NotFound();
            }

            return Ok(items);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(RssSourceDto), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var item = await _rssSourceService.GetItem(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // POST api/values
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> Post([FromBody] RssSourceDto item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            await _rssSourceService.Add(item);

            return Ok();
        }

        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> Put([FromBody] RssSourceDto item)
        {
            if (item == null || await _rssSourceService.GetItem(item.Id) == null)
            {
                return BadRequest();
            }

            await _rssSourceService.Update(item);

            return Ok();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _rssSourceService.GetItem(id);
            if (item == null)
            {
                return BadRequest();
            }

            await _rssSourceService.Remove(item);

            return Ok();
        }
    }
}