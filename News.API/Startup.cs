﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using News.API.Util;
using News.Core.Services;
using News.Core.Util;
using News.Data.EF;
using News.Data.Interfaces;
using News.Data.Repositories;
using News.Data.UnitsOfWork;
using Swashbuckle.AspNetCore.Swagger;

namespace News.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var authOptions = new AuthOptions("News web api", "http://localhost:8080", 60 * 24 * 3, "mysupersecret_secretkey!123");
            services.AddSingleton<AuthOptions>(opt => authOptions);

            services.AddMvc().AddXmlSerializerFormatters();

            services.AddSwaggerGen(opt => opt.SwaggerDoc("v1", new Info {Title = "NewsAPI", Version = "v1"}));
            services.AddAutoMapper(opt => opt.AddProfile(new AutomapperConfig()));

            services.AddDbContext<NewsContext>(opt =>
                                                   opt.UseSqlServer(
                                                       Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<RepositoryFactory>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<NewsService>();
            services.AddScoped<CategoryService>();
            services.AddScoped<RssSourceService>();

            services.AddScoped<UserService>();

            services.AddScoped<CommentService>();
            services.AddScoped<SavedNewsService>();
            services.AddScoped<SubscribeService>();
            
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = authOptions.Issuer,
                        
                        ValidateAudience = true,
                        ValidAudience = authOptions.Audience,

                        ValidateLifetime = true,

                        IssuerSigningKey = authOptions.GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true,
                    };
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder.AllowAnyOrigin()
                                          .AllowAnyMethod()
                                          .AllowCredentials()
                                          .AllowAnyHeader());

            app.UseSwagger();
            app.UseSwaggerUI(opt => opt.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"));

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}