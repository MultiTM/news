﻿using NLog.Config;
using NLog.Targets;

namespace News.BLL.Configs
{
    public static class Settings
    {
        public static class RssReader
        {
            public static LoggingConfiguration GetLoggerConfiguration()
            {
                var loggerConfig = new LoggingConfiguration();
                var consoleTarget = new ColoredConsoleTarget("consoleTarget")
                {
                    Layout = @"${date:format=HH\:mm\:ss} ${level} ${message} ${exception}"
                };

                var fileTarget = new FileTarget("fileTarget")
                {
                    Layout = @"${date:format=HH\:mm\:ss} ${level} ${message} ${exception}",
                    FileName = "${basedir}/logs/file.txt"
                };

                loggerConfig.AddTarget(consoleTarget);
                loggerConfig.AddTarget(fileTarget);

                loggerConfig.AddRuleForAllLevels(consoleTarget);
                loggerConfig.AddRuleForAllLevels(fileTarget);

                return loggerConfig;
            }
        }
    }
}
