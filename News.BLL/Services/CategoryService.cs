﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.BLL.Util;
using News.DAL.Entities;
using News.DAL.Interfaces;

namespace News.BLL.Services
{
    public class CategoryService : IService<CategoryDto>
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public CategoryService(IUnitOfWork uow, IMapper mapper)
        {
            _database = uow;
            _mapper = mapper;
        }
        public async Task Add(CategoryDto itemDto)
        {
            var item = _mapper.Map<Category>(itemDto);

            _database.Categories.Add(item);
            await _database.Save();
        }

        public async Task Add(List<CategoryDto> itemDtoList)
        {
            foreach (var itemDto in itemDtoList)
            {
                if (itemDto != null)
                {
                    var item = _mapper.Map<Category>(itemDto);
                    _database.Categories.Add(item);
                }
            }

            await _database.Save();
        }

        public async Task Delete(CategoryDto itemDto)
        {
            var item = _mapper.Map<Category>(itemDto);
            await _database.Categories.Delete(item);
            await _database.Save();
        }

        public async Task Edit(CategoryDto itemDto)
        {
            var item = _mapper.Map<Category>(itemDto);

            var inDb = await _database.Categories.Get(item.Id);
            if (inDb == null)
            {
                return;
            }
            
            await _database.Categories.Update(item);
            await _database.Save();
        }

        public async Task<CategoryDto> GetItem(int id)
        {
            var category = await _database.Categories.Get(id);
            if (category == null)
            {
                return null;
            }
            return _mapper.Map<CategoryDto>(category);
        }

        public async Task<List<CategoryDto>> GetAllItemsAsync()
        {
            var items = await _database.Categories.GetItems().ToListAsync();
            return _mapper.Map<List<CategoryDto>>(items);
        }

        public async Task<PageDto<CategoryDto>> GetPageAsync(PageInfoDto pageInfo)
        {
            var query = _database.Categories.GetItems().ProjectTo<CategoryDto>(_mapper.ConfigurationProvider);

            return await CreatePage(query, pageInfo);
        }

        private async Task<PageDto<CategoryDto>> CreatePage(IQueryable<CategoryDto> query, PageInfoDto pageInfo)
        {
            pageInfo.ItemCount = await query.CountAsync();

            query = query.OrderBy(pageInfo.SortingField, pageInfo.IsSortingAscending);

            if (pageInfo.CurrentPage > pageInfo.PageCount)
            {
                pageInfo.CurrentPage = pageInfo.PageCount == 0 ? 1 : pageInfo.PageCount;
                pageInfo.CurrentPage = pageInfo.CurrentPage;
            }

            query = query.Page(pageInfo.CurrentPage, pageInfo.PageSize);

            var pageItem = new PageDto<CategoryDto>()
            {
                PageInfo = pageInfo,
                Items = await query.ToListAsync()
            };

            return pageItem;
        }
    }
}
