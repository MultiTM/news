﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.BLL.Util;
using News.DAL.Entities;
using News.DAL.Interfaces;

namespace News.BLL.Services
{
    public class RssSourceService : IService<RssSourceDto>
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public RssSourceService(IUnitOfWork uow, IMapper mapper)
        {
            _database = uow;
            _mapper = mapper;
        }
        public async Task Add(RssSourceDto itemDto)
        {
            var item = _mapper.Map<RssSource>(itemDto);

            _database.RssSources.Add(item);
            await _database.Save();
        }

        public async Task Add(List<RssSourceDto> itemDtoList)
        {
            foreach (var itemDto in itemDtoList)
            {
                if (itemDto != null)
                {
                    var item = _mapper.Map<RssSource>(itemDto);
                    _database.RssSources.Add(item);
                }
            }

            await _database.Save();
        }

        public async Task Delete(RssSourceDto itemDto)
        {
            var item = _mapper.Map<RssSource>(itemDto);
            await _database.RssSources.Delete(item);
            await _database.Save();
        }

        public async Task Edit(RssSourceDto itemDto)
        {
            var item = _mapper.Map<RssSource>(itemDto);

            var inDb = await _database.RssSources.Get(item.Id);
            if (inDb == null)
            {
                return;
            }

            await _database.RssSources.Update(item);
            await _database.Save();
        }

        public async Task<RssSourceDto> GetItem(int id)
        {
            var category = await _database.RssSources.Get(id);
            if (category == null)
            {
                return null;
            }
            return _mapper.Map<RssSourceDto>(category);
        }

        public async Task<List<RssSourceDto>> GetAllItemsAsync()
        {
            var items = await _database.RssSources.GetItems().ToListAsync();
            return _mapper.Map<List<RssSourceDto>>(items);
        }

        public async Task<PageDto<RssSourceDto>> GetPageAsync(PageInfoDto pageInfo)
        {
            var query = _database.RssSources.GetItems().ProjectTo<RssSourceDto>(_mapper.ConfigurationProvider);

            return await CreatePage(query, pageInfo);
        }

        private async Task<PageDto<RssSourceDto>> CreatePage(IQueryable<RssSourceDto> query, PageInfoDto pageInfo)
        {
            pageInfo.ItemCount = await query.CountAsync();

            query = query.OrderBy(pageInfo.SortingField, pageInfo.IsSortingAscending);

            if (pageInfo.CurrentPage > pageInfo.PageCount)
            {
                pageInfo.CurrentPage = pageInfo.PageCount == 0 ? 1 : pageInfo.PageCount;
                pageInfo.CurrentPage = pageInfo.CurrentPage;
            }

            query = query.Page(pageInfo.CurrentPage, pageInfo.PageSize);

            var pageItem = new PageDto<RssSourceDto>()
            {
                PageInfo = pageInfo,
                Items = await query.ToListAsync()
            };

            return pageItem;
        }
    }
}
