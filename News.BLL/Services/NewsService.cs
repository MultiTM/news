﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using News.BLL.Configs;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.BLL.Util;
using News.DAL.Entities;
using News.DAL.Exceptions;
using News.DAL.Interfaces;
using NLog;

namespace News.BLL.Services
{
    public class NewsService : INewsService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public NewsService(IUnitOfWork uow, IMapper mapper)
        {
            _database = uow;
            _mapper = mapper;

            _logger = LogManager.GetCurrentClassLogger();
            _logger.Factory.Configuration = Settings.RssReader.GetLoggerConfiguration();
        }

        public async Task<NewsItemDto> GetItem(int id)
        {
            var newsItem = await _database.News.Get(id);
            if (newsItem == null)
            {
                return null;
            }
            return _mapper.Map<NewsItemDto>(newsItem);
        }

        public Task Add(NewsItemDto itemDto)
        {
            var item = _mapper.Map<NewsItem>(itemDto);
            if (item.Date == DateTime.MinValue)
            {
                item.Date = DateTime.Now;
            }
            
            _database.News.Add(item);
            return _database.Save();
        }

        public async Task Add(List<NewsItemDto> itemDtoList)
        {
            try
            {
                foreach (var itemDto in itemDtoList)
                {
                    try
                    {
                        var item = _mapper.Map<NewsItem>(itemDto);
                        _database.News.Add(item);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        _logger.Error($"Error occured while adding news item to database, news item was skipped");
                    }
                }

                await _database.Save();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                _logger.Error($"Error occured while saving news to database, news wasn't saved");
            }
        }

        public async Task Edit (NewsItemDto itemDto)
        {
            var item = _mapper.Map<NewsItem>(itemDto);

            var inDb = await _database.News.Get(item.Id);
            if (inDb == null)
            {
                throw new ItemEditException("Edit error");
            }

            item.Date = inDb.Date;
            
            if (!inDb.IsPublished && item.IsPublished)
            {
                item.Date = DateTime.Now;
            }
            await _database.News.Update(item);
            await _database.Save();
        }

        public async Task Delete (NewsItemDto itemDto)
        {
            var item = _mapper.Map<NewsItem>(itemDto);
            await _database.News.Delete(item);
            await _database.Save();
        }

        public async Task<PageDto<NewsItemDto>> GetPageAsync(PageInfoDto pageInfo, bool publishedOnly)
        {
            var query = _database.News.GetItems().ProjectTo<NewsItemDto>(_mapper.ConfigurationProvider);

            if (publishedOnly)
            {
                query = query.Where(n => n.IsPublished);
            }

            if (pageInfo.Category != null)
            {
                var categoryItem = await _database.Categories.GetItems().FirstOrDefaultAsync(c => c.Name == pageInfo.Category);

                if (categoryItem == null)
                {
                    query = query.Where(n => n.CategoryId == -1);
                }
                else
                {
                    query = query.Where(n => n.CategoryId == categoryItem.Id);
                }
            }
            
            return await CreatePage(query, pageInfo);
        }

        public DateTime GetLatestDateNewsWithSourceId(int sourceId)
        {
            var query = _database.News.GetItems().Where(n => n.RssSourceId == sourceId);
            var date = _database.News.GetItems().Where(n => n.RssSourceId == sourceId).Max(n => n.Date);
            var lastNewsItem = query.OrderByDescending(n => n.Date).FirstOrDefault();
            if (lastNewsItem == null)
            {
                return DateTime.MinValue;
            }
            else
            {
                return lastNewsItem.Date;
            }
        }

        public async Task<PageDto<NewsItemDto>> GetPageAsync(PageInfoDto pageInfo)
        {
            var query = _database.News.GetItems().ProjectTo<NewsItemDto>(_mapper.ConfigurationProvider);

            if (pageInfo.Category != null)
            {
                var categoryItem = await _database.Categories.GetItems().FirstOrDefaultAsync(c => c.Name == pageInfo.Category);

                if (categoryItem == null)
                {
                    query = query.Where(n => n.CategoryId == -1);
                }
                else
                {
                    query = query.Where(n => n.CategoryId == categoryItem.Id);
                }
            }

            return await CreatePage(query, pageInfo);
        }

        private async Task<PageDto<NewsItemDto>> CreatePage(IQueryable<NewsItemDto> query, PageInfoDto pageInfo)
        {
            pageInfo.ItemCount = await query.CountAsync();

            if (pageInfo.CurrentPage > pageInfo.PageCount)
            {
                pageInfo.CurrentPage = pageInfo.PageCount == 0 ? 1 : pageInfo.PageCount;
                pageInfo.CurrentPage = pageInfo.CurrentPage;
            }

            if (pageInfo.SortingField != null)
            {
                query = query.OrderBy(pageInfo.SortingField, pageInfo.IsSortingAscending);
            }

            query = query.Page(pageInfo.CurrentPage, pageInfo.PageSize);

            var pageItem = new PageDto<NewsItemDto>()
            {
                PageInfo = pageInfo,
                Items = await query.ToListAsync()
            };

            return pageItem;
        }

        public async Task<List<NewsItemDto>> GetAllItemsAsync()
        {
            var items = await _database.News.GetItems().ToListAsync();
            return _mapper.Map<List<NewsItemDto>>(items);
        }
    }
}
