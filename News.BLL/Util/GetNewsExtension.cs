﻿using System.Linq;
using System.Linq.Expressions;
using News.BLL.DTO;

namespace News.BLL.Util
{
    public static class GetNewsExtensions
    {
        public static IQueryable<T> Page<T>(this IQueryable<T> query, int page, int pageSize = 10)
        {
            return query.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public static IQueryable<NewsItemDto> Filter(this IQueryable<NewsItemDto> query, string filter)
        {
            return query.Where(n => n.Title.Contains(filter));
        }

        public static IQueryable<CategoryDto> Filter(this IQueryable<CategoryDto> query, string filter)
        {
            return query.Where(n => n.Name.Contains(filter));
        }

        public static IQueryable<RssSourceDto> Filter(this IQueryable<RssSourceDto> query, string filter)
        {
            return query.Where(n => n.Name.Contains(filter));
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> query, string sortingField, bool isSortingAscending = true)
        {

            var searchProperty = typeof(T).GetProperty(sortingField);

            var parameter = Expression.Parameter(typeof(T), "o");
            var selectorExpr = Expression.Lambda(Expression.Property(parameter, sortingField), parameter);

            var queryExpr = query.Expression;
            queryExpr = Expression.Call(
                typeof(Queryable),
                isSortingAscending ? "OrderBy" : "OrderByDescending",
                new[] {
                    query.ElementType,
                    searchProperty.PropertyType },
                queryExpr,
                selectorExpr);

            return query.Provider.CreateQuery<T>(queryExpr);
        }
    }
}
