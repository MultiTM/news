﻿using System.Collections.Generic;
using System.Threading.Tasks;
using News.BLL.DTO;

namespace News.BLL.Interfaces
{
    public interface IService<T> where T : IDto
    {
        Task<T> GetItem(int id);
        Task Add(T itemDto);
        Task Add(List<T> itemDtoList);
        Task Edit(T itemDto);
        Task Delete(T itemDto);
        Task<PageDto<T>> GetPageAsync(PageInfoDto pageInfo);
        Task<List<T>> GetAllItemsAsync();
    }
}
