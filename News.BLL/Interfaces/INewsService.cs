﻿using System;
using System.Threading.Tasks;
using News.BLL.DTO;

namespace News.BLL.Interfaces
{
    public interface INewsService : IService<NewsItemDto>
    {
        Task<PageDto<NewsItemDto>> GetPageAsync(PageInfoDto pageInfo, bool publishedOnly);

        DateTime GetLatestDateNewsWithSourceId(int sourceId);
    }
}
