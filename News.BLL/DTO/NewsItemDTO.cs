﻿using System;
using News.BLL.Interfaces;

namespace News.BLL.DTO
{
    public class NewsItemDto : IDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public bool IsPublished { get; set; }
        public DateTime Date { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool IsExternal { get; set; }
        public string ExternalLink { get; set; }
        public int? SourceId { get; set; }
        public string SourceName { get; set; }
    }
}
