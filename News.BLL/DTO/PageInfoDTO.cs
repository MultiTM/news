﻿using System;
using News.BLL.Interfaces;

namespace News.BLL.DTO
{
    public class PageInfoDto : IDto
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int ItemCount { get; set; }
        public int PageCount => (int)Math.Ceiling((decimal)ItemCount / PageSize);
        public string SortingField { get; set; }
        public bool IsSortingAscending { get; set; }
        public string Filter { get; set; }

        public string Category { get; set; }

        public PageInfoDto(int itemCount, int currentPage = 1, int pageSize = 10, string sortingField = null, bool isSortingAscending = false, string filter = null, string category = null)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            ItemCount = itemCount;
            SortingField = sortingField;
            IsSortingAscending = isSortingAscending;
            Filter = filter;
            Category = category;
        }
        public PageInfoDto(int itemCount, int currentPage = 1, int pageSize = 10, string sortingField = null, bool isSortingAscending = false, string filter = null)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            ItemCount = itemCount;
            SortingField = sortingField;
            IsSortingAscending = isSortingAscending;
            Filter = filter;
        }

        public PageInfoDto()
        {
            CurrentPage = 1;
            PageSize = 10;
            ItemCount = 0;
            SortingField = null;
            IsSortingAscending = false;
            Filter = null;
            Category = null;
        }
    }
}