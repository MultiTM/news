﻿using News.BLL.Interfaces;

namespace News.BLL.DTO
{
    public class CategoryDto : IDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
