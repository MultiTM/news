﻿using News.BLL.Interfaces;

namespace News.BLL.DTO
{
    public class RssSourceDto : IDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string RssLink { get; set; }
    }
}
