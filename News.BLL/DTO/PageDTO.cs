﻿using System.Collections.Generic;
using News.BLL.Interfaces;

namespace News.BLL.DTO
{
    public class PageDto<T> where T : IDto
    {
        public PageInfoDto PageInfo { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
