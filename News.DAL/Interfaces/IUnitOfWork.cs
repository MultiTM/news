﻿using News.DAL.Repositories;
using System.Threading.Tasks;

namespace News.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        NewsRepository News { get; }
        CategoryRepository Categories { get; }
        RssSourceRepository RssSources { get; }
        Task Save();
    }
}
