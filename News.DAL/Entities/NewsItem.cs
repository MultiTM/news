﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace News.DAL.Entities
{
    public class NewsItem
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public bool IsPublished { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public Category Category { get; set; }

        public RssSource Source { get; set; }

        [Required]
        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        [ForeignKey("Source")]
        public int? RssSourceId { get; set; }

        [Required]
        public bool IsExternal { get; set; }

        public string ExternalLink { get; set; }
    }
}
