﻿namespace News.DAL.Entities
{
    public class RssSource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public  string RssLink { get; set; }
    }
}
