﻿using News.DAL.EF;
using News.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using News.DAL.Exceptions;

namespace News.DAL.Repositories
{
    public class NewsRepository : IRepository<NewsItem>
    {
        private readonly NewsContext _db;
        public NewsRepository(NewsContext db) 
        {
            _db = db;
        }
        
        public IQueryable<NewsItem> GetItems()
        {
            return _db.News;
        }
        public async Task<NewsItem> Get(int id)
        {
            return await _db.News.FindAsync(id);
        }
        public IEnumerable<NewsItem> Find(Func<NewsItem, Boolean> predicate)
        {
            return _db.News.Where(predicate);
        }
        public void Add(NewsItem item)
        {
            _db.News.Add(item);
        }
        public async Task Update(NewsItem item)
        {
            var entity = await _db.News.FindAsync(item.Id);
            if (entity != null)
            {
                _db.Entry(entity).CurrentValues.SetValues(item);
            }
        }
        public async Task Delete(NewsItem item)
        {
            var newsItem = await _db.News.FindAsync(item.Id);
            if (newsItem == null)
            {
                throw new ItemEditException("Delete error");
            }
            _db.Entry(newsItem).State = EntityState.Deleted;
        }
    }
}
