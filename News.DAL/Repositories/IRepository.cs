﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace News.DAL.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetItems();
        Task<T> Get(int id);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Add(T item);
        Task Update(T item);
        Task Delete(T item);
    }
}
