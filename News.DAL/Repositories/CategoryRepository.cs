﻿using News.DAL.EF;
using News.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace News.DAL.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly NewsContext _db;

        public CategoryRepository(NewsContext db)
        {
            _db = db;
        }
        public void Add(Category item)
        {
            _db.Categories.Add(item);
        }

        public async Task Delete(Category item)
        {
            var categoryItem = await _db.Categories.FindAsync(item.Id);
            {
                _db.Entry(categoryItem).State = EntityState.Deleted;
            }
        }

        public IEnumerable<Category> Find(Func<Category, bool> predicate)
        {
            return _db.Categories.Where(predicate);
        }

        public async Task<Category> Get(int id)
        {
            return await _db.Categories.FindAsync(id);
        }

        public IQueryable<Category> GetItems()
        {
            return _db.Categories;
        }

        public async Task Update(Category item)
        {
            var entity = await _db.Categories.FindAsync(item.Id);
            if (entity != null)
            {
                _db.Entry(entity).CurrentValues.SetValues(item);
            }
        }
    }
}
