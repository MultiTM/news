﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using News.DAL.EF;
using News.DAL.Entities;

namespace News.DAL.Repositories
{
    public class RssSourceRepository : IRepository<RssSource>
    {
        private readonly NewsContext _db;

        public RssSourceRepository(NewsContext db)
        {
            _db = db;
        }
        public void Add(RssSource item)
        {
            _db.RssSources.Add(item);
        }

        public async Task Delete(RssSource item)
        {
            var rssSourceItem = await _db.RssSources.FindAsync(item.Id);
            {
                _db.Entry(rssSourceItem).State = EntityState.Deleted;
            }
        }

        public IEnumerable<RssSource> Find(Func<RssSource, bool> predicate)
        {
            return _db.RssSources.Where(predicate);
        }

        public async Task<RssSource> Get(int id)
        {
            return await _db.RssSources.FindAsync(id);
        }

        public IQueryable<RssSource> GetItems()
        {
            return _db.RssSources;
        }

        public async Task Update(RssSource item)
        {
            var entity = await _db.RssSources.FindAsync(item.Id);
            if (entity != null)
            {
                _db.Entry(entity).CurrentValues.SetValues(item);
            }
        }
    }
}
