﻿using News.DAL.EF;
using News.DAL.Interfaces;
using System.Threading.Tasks;

namespace News.DAL.Repositories
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private readonly NewsContext _newsContext;
        private NewsRepository _newsRepository;
        private CategoryRepository _categoryRepository;
        private RssSourceRepository _rssSourceRepository;

        public NewsRepository News
        {
            get
            {
                if (_newsRepository == null)
                {
                    _newsRepository = new NewsRepository(_newsContext);
                }
                return _newsRepository;
            }
        }

        public CategoryRepository Categories
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new CategoryRepository(_newsContext);
                }
                return _categoryRepository;
            }
        }

        public RssSourceRepository RssSources
        {
            get
            {
                if (_rssSourceRepository == null)
                {
                    _rssSourceRepository = new RssSourceRepository(_newsContext);
                }

                return _rssSourceRepository;
            }
        }

        public EfUnitOfWork(NewsContext context)
        {
            _newsContext = context;
        }

        public async Task Save()
        {
            await _newsContext.SaveChangesAsync();
        }
    }
}
