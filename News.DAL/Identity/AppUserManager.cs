﻿using Microsoft.AspNet.Identity;
using News.DAL.Entities;

namespace News.DAL.Identity
{
    public class AppUserManager : UserManager<AppUser>
    {
        public AppUserManager(IUserStore<AppUser> store) : base(store)
        {}
    }
}
