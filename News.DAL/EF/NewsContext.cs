﻿using Microsoft.AspNet.Identity.EntityFramework;
using News.DAL.Entities;
using System.Data.Entity;
using System.Diagnostics;

namespace News.DAL.EF
{
    public class NewsContext : IdentityDbContext<AppUser>
    {
        public DbSet<NewsItem> News { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<RssSource> RssSources { get; set; }

        public NewsContext()
            : base("name=NewsBase")
        {
            Database.Log = (log) => Trace.WriteLine(log);
        }
    }
}
