﻿using System;

namespace News.DAL.Exceptions
{
    public class ItemEditException : ApplicationException
    {
        public ItemEditException(string message) : base(message)
        {
        }

        public ItemEditException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}