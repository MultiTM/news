﻿using Microsoft.EntityFrameworkCore;
using News.DALCore.Entities;
using News.DALCore.Interfaces;

namespace News.DALCore.Repositories
{
    class RssSourceRepository : Repository<RssSource>, IRssSourceRepository
    {
        public RssSourceRepository(DbContext context) : base(context)
        {
        }
    }
}
