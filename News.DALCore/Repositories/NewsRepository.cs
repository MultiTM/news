﻿using Microsoft.EntityFrameworkCore;
using News.DALCore.EF;
using News.DALCore.Entities;
using News.DALCore.Interfaces;

namespace News.DALCore.Repositories
{
    public class NewsRepository : Repository<NewsItem>, INewsRepository
    {
        public NewsRepository(DbContext context) : base(context)
        {
        }
    }
}
