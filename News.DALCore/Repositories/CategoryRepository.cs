﻿using Microsoft.EntityFrameworkCore;
using News.DALCore.Entities;
using News.DALCore.Interfaces;

namespace News.DALCore.Repositories
{
    class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbContext context) : base(context)
        {
        }
    }
}
