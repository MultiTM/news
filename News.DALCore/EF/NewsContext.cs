﻿using Microsoft.EntityFrameworkCore;
using News.DALCore.Entities;

namespace News.DALCore.EF
{
    public class NewsContext : DbContext
    {
        public DbSet<NewsItem> News { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<RssSource> RssSources { get; set; }
    }
}
