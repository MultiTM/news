﻿using News.DALCore.Entities;

namespace News.DALCore.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
