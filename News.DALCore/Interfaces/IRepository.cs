﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace News.DALCore.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> Get(int id);
        IEnumerable<T> Find(Func<T, Boolean> predicate);

        void Add(T item);
        void AddRange(IEnumerable<T> items);
        void Remove(T item);
        void RemoveRange(IEnumerable<T> items);
    }
}
