﻿using News.DALCore.Entities;

namespace News.DALCore.Interfaces
{
    public interface IRssSourceRepository : IRepository<RssSource>
    {
    }
}
