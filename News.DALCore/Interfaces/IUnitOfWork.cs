﻿using System;

namespace News.DALCore.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        INewsRepository News { get; }
        ICategoryRepository Categories { get; }
        IRssSourceRepository RssSources { get; }

        int Complete();
    }
}
