﻿using News.DALCore.Entities;

namespace News.DALCore.Interfaces
{
    public interface INewsRepository : IRepository<NewsItem>
    {
    }
}
