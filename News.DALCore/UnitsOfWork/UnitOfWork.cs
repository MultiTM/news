﻿using News.DALCore.EF;
using News.DALCore.Interfaces;
using News.DALCore.Repositories;

namespace News.DALCore.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly NewsContext _context;

        public UnitOfWork(NewsContext context)
        {
            _context = context;
            News = new NewsRepository(_context);
            Categories = new CategoryRepository(_context);
            RssSources = new RssSourceRepository(_context);
        }

        public INewsRepository News { get; }
        public ICategoryRepository Categories { get; }
        public IRssSourceRepository RssSources { get; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
