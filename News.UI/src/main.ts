import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'

import Vuetify from 'vuetify'
Vue.use(Vuetify);

import VeeValidate from 'vee-validate'
Vue.use(VeeValidate);

Vue.filter('date', function(val:string){
  return (new Date(Date.parse(val))).toDateString();
});

Vue.filter('truncate', function(val: string, length: number){
  if (val.length > length){
      return val.slice(0, length - 1);
  } else {
      return val;
  }
});

Vue.config.productionTip = false;

new Vue({
  data(){
    return {
      IsLogged: false,
      Url: 'https://localhost:44368/api/'
    }
  },
  computed:{
    username: function(){
      return localStorage.getItem("Username");
    },
    userId: function(){
      return localStorage.getItem("UserId");
    }
  },
  methods:{

    Login(user:any){
      this.$root.$emit('startLoading');
      axios.post(this.Url + "user/login", user)
        .then(response => {
          localStorage.setItem("Token", response.data.token);
          localStorage.setItem("Username", response.data.username);
          localStorage.setItem("UserId", response.data.userId);
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem("Token");
          this.IsLogged = true;
        })
        .catch(error => {
          localStorage.removeItem("Token");
          this.$root.$emit('login', 'Login error.');
        })
        .then(() => this.$root.$emit('endLoading'));
    },

    Logout(){
      localStorage.removeItem("Token");
      this.IsLogged = false;
      this.$router.push('/');
    },

    Register(user:any){
      this.$root.$emit('startLoading');
      axios.post(this.Url + 'user/register', user)
        .then(response => {})
        .catch(error => {
          this.$root.$emit('register', 'Registration error');
        })
        .then(() => this.$root.$emit('endLoading'));
    }

  },
  created(){
    this.IsLogged = localStorage.getItem("Token") !== null;
    if (this.IsLogged){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem("Token");
    }
  },
  router,
  render: h => h(App)
}).$mount('#app')
