import Vue from 'vue'
import Router from 'vue-router'

import News from './components/Core/Pages/News/News.vue'
import NewsDetail from './components/Core/Pages/News/NewsDetail.vue'

import UserSavedNews from './components/Core/Pages/UserPanel/UserSavedNews.vue'
import UserSubscribes from './components/Core/Pages/UserPanel/UserSubscribes.vue'
import UserComments from './components/Core/Pages/UserPanel/UserComments.vue'

Vue.use(Router)

let router =  new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: News,
      meta: {
        search: false,
      },
      children:[
        {
          path: 'login',
          name: 'login',
          component: News,
          meta: {
            login: true
          }
        },
        {
          path: 'search/:searchString',
          name: 'search',
          component: News,
          meta:{
            search: true
          }
        }
      ]
    },
    {
      path: '/detail/:id',
      name: 'news-detail',
      component: NewsDetail
    },

    {
      path: '/user/saved',
      name: 'user-saved',
      component: UserSavedNews,
      meta: { requiresAuth: true }
    },
    {
      path: '/user/subscribes',
      name: 'user-subscribes',
      component: UserSubscribes,
      meta: { requiresAuth: true }
    },
    {
      path: '/user/comments',
      name: 'user-comments',
      component: UserComments,
      meta: { requiresAuth: true }
    },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("Token") === null) {
      next('/login');
    } else {
      next();
    }
  } else {
    next();
  }
})

export default router;