﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using News.BLL.DTO;
using News.BLL.Interfaces;
using RssReader.BL;

namespace RssReader.Tests.BL
{
    [TestClass]
    public class RssTransceiverTests
    {
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task GetFeed_NullSourcePassed_ThrowApplicationException()
        {
            var newsServiceMock = new Mock<INewsService>();

            var rssTransceiver = new RssTransceiver(newsServiceMock.Object);
            await rssTransceiver.GetFeed(null);
        }

        [TestMethod]
        public async Task GetFeed_SourcePassed_ReturnNewsList()
        {
            var url = "https://news.tut.by/rss/all.rss";
            var sourceId = 4;

            var newsServiceMock = new Mock<INewsService>();
            newsServiceMock.Setup(x => x.GetLatestDateNewsWithSourceId(sourceId)).Returns(DateTime.MinValue);

            var rssTransceiver = new RssTransceiver(newsServiceMock.Object);
            var newsList = await rssTransceiver.GetFeed(new RssSourceDto{ RssLink = url, Id = sourceId});

            Assert.IsNotNull(newsList);
        }
    }
}
