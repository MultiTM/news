﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeHollow.FeedReader;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using News.BLL.DTO;
using News.BLL.Interfaces;
using RssReader.BL;

namespace RssReader.Tests.BL
{
    [TestClass]
    public class CategoryResolverTests
    {
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task AddAndUpdateCategoriesInCache_NullListPassed_ThrowApplicationException()
        {
            var cacheList = new List<CategoryDto>
            {
                new CategoryDto(){Name = "cat1"}
            };
            
            var categoryServiceMock = new Mock<IService<CategoryDto>>();
            categoryServiceMock.Setup(x => x.Add(It.IsAny<CategoryDto>())).Returns(Task.CompletedTask);
            categoryServiceMock.Setup(x => x.GetAllItemsAsync()).ReturnsAsync(cacheList);

            var categoryResolver = new CategoryResolver(categoryServiceMock.Object);
            await categoryResolver.AddAndUpdateCategoriesInCache(null);
        }

        [TestMethod]
        public async Task AddAndUpdateCategoriesInCache_NewCategoriesPassed_InvokeAdd()
        {
            var cacheList = new List<CategoryDto>
            {
                new CategoryDto(){Name = "cat1"}
            };

            var inputList = new List<FeedItem>
            {
                new FeedItem(){Categories = new List<string>{"cat1"}},
                new FeedItem(){Categories = new List<string>{"cat1"}},
                new FeedItem(){Categories = new List<string>{"cat2"}}
            };

            var categoryServiceMock = new Mock<IService<CategoryDto>>();
            categoryServiceMock.Setup(x => x.Add(It.IsAny<CategoryDto>())).Returns(Task.CompletedTask).Verifiable();
            categoryServiceMock.Setup(x => x.GetAllItemsAsync()).ReturnsAsync(cacheList);

            var categoryResolver = new CategoryResolver(categoryServiceMock.Object);
            await categoryResolver.AddAndUpdateCategoriesInCache(inputList);

            categoryServiceMock.Verify();
        }

        [TestMethod]
        public async Task AddAndUpdateCategoriesInCache_NoNewCategoriesPassed_AddNotInvoked()
        {
            var cacheList = new List<CategoryDto>
            {
                new CategoryDto(){Name = "cat1"}
            };

            var inputList = new List<FeedItem>
            {
                new FeedItem(){Categories = new List<string>{"cat1"}},
                new FeedItem(){Categories = new List<string>{"cat1"}}
            };

            var categoryServiceMock = new Mock<IService<CategoryDto>>();
            categoryServiceMock.Setup(x => x.Add(It.IsAny<CategoryDto>())).Returns(Task.CompletedTask);
            categoryServiceMock.Setup(x => x.GetAllItemsAsync()).ReturnsAsync(cacheList);

            var categoryResolver = new CategoryResolver(categoryServiceMock.Object);
            await categoryResolver.AddAndUpdateCategoriesInCache(inputList);

            categoryServiceMock.Verify(x => x.Add(It.IsAny<CategoryDto>()), Times.Never);
        }

        [TestMethod]
        public void GetCategoryId_CategoryNamePassed_ReturnCategoryIdWithPassedName()
        {
            var cacheList = new List<CategoryDto>
            {
                new CategoryDto(){Name = "cat1", Id = 1},
                new CategoryDto(){Name = "cat2", Id = 2}
            };

            var categoryName = "cat2";

            var categoryServiceMock = new Mock<IService<CategoryDto>>();
            categoryServiceMock.Setup(x => x.GetAllItemsAsync()).ReturnsAsync(cacheList);

            var categoryResolver = new CategoryResolver(categoryServiceMock.Object);
            var id = categoryResolver.GetCategoryId(categoryName);

            Assert.AreEqual(2, id);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetCategoryId_NullCategoryName_ThrowApplicationException()
        {
            var cacheList = new List<CategoryDto>
            {
                new CategoryDto(){Name = "cat1", Id = 1},
                new CategoryDto(){Name = "cat2", Id = 2}
            };
            
            var categoryServiceMock = new Mock<IService<CategoryDto>>();
            categoryServiceMock.Setup(x => x.GetAllItemsAsync()).ReturnsAsync(cacheList);

            var categoryResolver = new CategoryResolver(categoryServiceMock.Object);
            var id = categoryResolver.GetCategoryId(null);
        }
    }
}
