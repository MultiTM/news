﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CodeHollow.FeedReader;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using News.BLL.DTO;
using News.BLL.Interfaces;
using RssReader.Interfaces;

namespace RssReader.Tests.BL
{
    [TestClass]
    public class RssReaderTests
    {
        [TestMethod]
        public async Task DownloadAndSaveNews_ArgsPassed_2ItemsAdded()
        {
            List<NewsItemDto> savedList = null;

            var feedList = new List<FeedItem>()
            {
                new FeedItem(){Description = "Text", Title = "Title", PublishingDate = DateTime.Today, Link = "some link", Categories = new List<string>{"cat1"}},
                new FeedItem(){Description = "Text", Title = "Title", PublishingDate = DateTime.Today, Link = "some link", Categories = new List<string>{"cat1"}},
            };

            var newsServiceMock = new Mock<INewsService>();
            newsServiceMock.Setup(x => x.Add(It.IsNotNull<List<NewsItemDto>>())).Callback<List<NewsItemDto>>(x => savedList = x).Returns(Task.CompletedTask);

            var categoryResolverMock = new Mock<ICategoryResolver>();
            categoryResolverMock.Setup(x => x.AddAndUpdateCategoriesInCache(It.IsNotNull<List<FeedItem>>()))
                .Returns(Task.CompletedTask);
            categoryResolverMock.Setup(x => x.GetCategoryId(It.IsNotNull<string>())).Returns(4);

            var rssTransceiverMock = new Mock<IRssTransceiver>();
            rssTransceiverMock.Setup(x => x.GetFeed(It.IsNotNull<RssSourceDto>())).ReturnsAsync(feedList);

            var rssReader = new RssReader.BL.RssReader(newsServiceMock.Object, categoryResolverMock.Object, rssTransceiverMock.Object);
            await rssReader.DownloadAndSaveNews(new RssSourceDto());
            
            newsServiceMock.Verify(x=>x.Add(It.IsNotNull<List<NewsItemDto>>()), Times.Once);
            Assert.AreEqual(2, savedList.Count);
        }

        [TestMethod]
        public async Task DownloadAndSaveNews_2NewsHasNotAllProperties_2ItemsAdded()
        {
            List<NewsItemDto> savedList = null;

            var feedList = new List<FeedItem>()
            {
                new FeedItem(){Description = "Text", Title = "Title", PublishingDate = DateTime.Today, Link = "some link", Categories = new List<string>{"cat1"}},
                new FeedItem(){Description = "Text", Title = "Title", PublishingDate = DateTime.Today, Link = "some link", Categories = new List<string>{"cat1"}},
                new FeedItem(),
                new FeedItem(),
            };

            var newsServiceMock = new Mock<INewsService>();
            newsServiceMock.Setup(x => x.Add(It.IsNotNull<List<NewsItemDto>>())).Callback<List<NewsItemDto>>(x => savedList = x).Returns(Task.CompletedTask);

            var categoryResolverMock = new Mock<ICategoryResolver>();
            categoryResolverMock.Setup(x => x.AddAndUpdateCategoriesInCache(It.IsNotNull<List<FeedItem>>()))
                .Returns(Task.CompletedTask);
            categoryResolverMock.Setup(x => x.GetCategoryId(It.IsNotNull<string>())).Returns(4);

            var rssTransceiverMock = new Mock<IRssTransceiver>();
            rssTransceiverMock.Setup(x => x.GetFeed(It.IsNotNull<RssSourceDto>())).ReturnsAsync(feedList);

            var rssReader = new RssReader.BL.RssReader(newsServiceMock.Object, categoryResolverMock.Object, rssTransceiverMock.Object);
            await rssReader.DownloadAndSaveNews(new RssSourceDto());

            newsServiceMock.Verify(x => x.Add(It.IsNotNull<List<NewsItemDto>>()), Times.Once);
            Assert.AreEqual(2, savedList.Count);
        }
    }
}
