﻿using Microsoft.Extensions.Logging;

namespace News.RssReader.Logging
{
    public static class FileLoggerExtensions
    {
        public static ILoggerFactory AddFile(this ILoggerFactory factory, string filepath)
        {
            factory.AddProvider(new FileLoggerProvider(filepath));

            return factory;
        }
    }
}