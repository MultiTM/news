﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeHollow.FeedReader;
using News.Core.DTO;
using News.Core.Interfaces;
using News.RssReader.Interfaces;

namespace News.RssReader.BL
{
    public class RssTransceiver : IRssTransceiver
    {
        private readonly INewsService _newsService;

        public RssTransceiver(INewsService newsService)
        {
            _newsService = newsService;
        }

        public async Task<List<FeedItem>> GetFeed(RssSourceDto source)
        {
            if (source == null || string.IsNullOrEmpty(source.RssLink))
            {
                return null;
            }

            var feed = await FeedReader.ReadAsync(source.RssLink);
            var latestNewsItem = await _newsService.GetLatestItemWithSourceId(source.Id);
            var lastDate = DateTime.MinValue;
            if (latestNewsItem != null)
            {
                lastDate = latestNewsItem.Date;
            }

            var news = feed.Items;
            var listNews = news.Where(i => i.PublishingDate > lastDate).ToList();

            return listNews;
        }
    }
}