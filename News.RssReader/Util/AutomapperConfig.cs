﻿using AutoMapper;
using News.Core.DTO;
using News.Data.Entities;

namespace News.RssReader.Util
{
    public class AutomapperConfig : Profile
    {
        public AutomapperConfig()
        {
            CreateMap<NewsItem, NewsItemDto>()
                .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name))
                .ForMember(dest => dest.RssSourceName, opt => opt.MapFrom(src => src.RssSource.Name));

            CreateMap<NewsItemDto, NewsItem>();

            CreateMap<Category, CategoryDto>();
            CreateMap<CategoryDto, Category>();

            CreateMap<RssSource, RssSourceDto>();
            CreateMap<RssSourceDto, RssSource>();
        }
    }
}