﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CodeHollow.FeedReader;

namespace News.RssReader.Interfaces
{
    public interface ICategoryResolver
    {
        int GetCategoryId(string category);

        Task AddAndUpdateCategoriesInCache(List<FeedItem> newsList);
    }
}