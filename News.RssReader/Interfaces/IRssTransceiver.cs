﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CodeHollow.FeedReader;
using News.Core.DTO;

namespace News.RssReader.Interfaces
{
    public interface IRssTransceiver
    {
        Task<List<FeedItem>> GetFeed(RssSourceDto source);
    }
}