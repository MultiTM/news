﻿using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using News.Core.Interfaces;
using News.Core.Services;
using News.Data.EF;
using News.Data.Interfaces;
using News.Data.Repositories;
using News.Data.UnitsOfWork;
using News.RssReader.BL;
using News.RssReader.Interfaces;
using News.RssReader.Logging;
using News.RssReader.Util;

namespace News.RssReader
{
    internal class Program
    {
        private static void Main()
        {
            MainAsync().Wait();
        }

        private static async Task MainAsync()
        {
            var services = new ServiceCollection()
                           .AddLogging()
                           .AddAutoMapper(opt => opt.AddProfile(new AutomapperConfig()))
                           .AddDbContext<NewsContext>(opt =>
                                                          opt.UseSqlServer(
                                                              "Server=(localdb)\\MSSQLLocalDB;Database=Newsbase_2;Trusted_Connection=True"))
                           .AddSingleton<RepositoryFactory>()
                           .AddScoped<IUnitOfWork, UnitOfWork>()
                           .AddScoped<INewsService, NewsService>()
                           .AddScoped<ICategoryService, CategoryService>()
                           .AddScoped<IRssSourceService, RssSourceService>()
                           .AddScoped<IRssTransceiver, RssTransceiver>()
                           .AddScoped<ICategoryResolver, CategoryResolver>()
                           .AddScoped<Reader>()
                           .BuildServiceProvider();

            services.GetService<ILoggerFactory>().AddConsole((category, logLevel) =>
                                                                 category.Contains("News") &&
                                                                 logLevel >= LogLevel.Trace);

            services.GetService<ILoggerFactory>().AddFile(Path.Combine(Directory.GetCurrentDirectory(), "Log.txt"));

            var logger = services.GetService<ILoggerFactory>().CreateLogger<Program>();
            logger.LogInformation("App started");

            var rssReader = services.GetService<Reader>();
            var rssSourcesService = services.GetService<IRssSourceService>();
            var sources = await rssSourcesService.GetAllAsync();

            foreach (var source in sources)
            {
                await rssReader.DownloadAndSaveNews(source);
            }

            logger.LogInformation("Press any key to exit...");
            Console.ReadKey();
        }
    }
}