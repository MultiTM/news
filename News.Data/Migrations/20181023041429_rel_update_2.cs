﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace News.Data.Migrations
{
    public partial class rel_update_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId1",
                table: "Subscribes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId1",
                table: "SavedNews",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NewsItemId1",
                table: "Comments",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId1",
                table: "Comments",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscribes_UserId1",
                table: "Subscribes",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_SavedNews_UserId1",
                table: "SavedNews",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_NewsItemId1",
                table: "Comments",
                column: "NewsItemId1");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserId1",
                table: "Comments",
                column: "UserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_News_NewsItemId1",
                table: "Comments",
                column: "NewsItemId1",
                principalTable: "News",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Users_UserId1",
                table: "Comments",
                column: "UserId1",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SavedNews_Users_UserId1",
                table: "SavedNews",
                column: "UserId1",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Subscribes_Users_UserId1",
                table: "Subscribes",
                column: "UserId1",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_News_NewsItemId1",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Users_UserId1",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_SavedNews_Users_UserId1",
                table: "SavedNews");

            migrationBuilder.DropForeignKey(
                name: "FK_Subscribes_Users_UserId1",
                table: "Subscribes");

            migrationBuilder.DropIndex(
                name: "IX_Subscribes_UserId1",
                table: "Subscribes");

            migrationBuilder.DropIndex(
                name: "IX_SavedNews_UserId1",
                table: "SavedNews");

            migrationBuilder.DropIndex(
                name: "IX_Comments_NewsItemId1",
                table: "Comments");

            migrationBuilder.DropIndex(
                name: "IX_Comments_UserId1",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Subscribes");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "SavedNews");

            migrationBuilder.DropColumn(
                name: "NewsItemId1",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Comments");
        }
    }
}
