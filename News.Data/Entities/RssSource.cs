﻿using News.Data.Interfaces;

namespace News.Data.Entities
{
    public class RssSource : IEntity
    {
        public string Name { get; set; }
        public string RssLink { get; set; }
        public int Id { get; set; }

        public void Set(RssSource item)
        {
            Name = item.Name;
            RssLink = item.RssLink;
        }
    }
}