﻿using News.Data.Interfaces;

namespace News.Data.Entities
{
    public class Subscribe : IEntity
    {
        public int Id { get; set; }
        
        public int UserId { get; set; }
        public User User { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public void Set(Subscribe item)
        {
            User = item.User;
            Category = item.Category;
        }
    }
}
