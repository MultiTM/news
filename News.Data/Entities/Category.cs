﻿using System.Collections.Generic;
using News.Data.Interfaces;

namespace News.Data.Entities
{
    public class Category : IEntity
    {
        public string Name { get; set; }
        public int Id { get; set; }

        public void Set(Category item)
        {
            Name = item.Name;
        }
    }
}