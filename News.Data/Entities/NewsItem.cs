﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using News.Data.Interfaces;

namespace News.Data.Entities
{
    public class NewsItem : IEntity
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public bool IsPublished { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public int? RssSourceId { get; set; }
        public RssSource RssSource { get; set; }

        public IEnumerable<Comment> Comments { get; set; }

        [Required]
        public bool IsExternal { get; set; }

        public string ExternalLink { get; set; }
        public int Id { get; set; }

        public void Set(NewsItem item)
        {
            Title = item.Title;
            Text = item.Text;
            IsPublished = item.IsPublished;
            Date = item.Date;
            Category = item.Category;
            RssSource = item.RssSource;
            IsExternal = item.IsExternal;
            ExternalLink = item.ExternalLink;
        }
    }
}