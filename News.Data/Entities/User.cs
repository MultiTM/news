﻿using System.Collections.Generic;
using News.Data.Interfaces;
using News.Data.Util;

namespace News.Data.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public UserRoles Role { get; set; }

        public IEnumerable<Comment> Comments { get; set; }
        public IEnumerable<SavedNewsItem> SavedNews { get; set; }
        public IEnumerable<Subscribe> Subscribes { get; set; }

        public void Set(User item)
        {
            Username = item.Username;
            Password = item.Password;
        }
    }
}
