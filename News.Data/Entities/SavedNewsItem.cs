﻿using News.Data.Interfaces;

namespace News.Data.Entities
{
    public class SavedNewsItem : IEntity
    {
        public int Id { get; set; }
        
        public int UserId { get; set; }
        public User User { get; set; }

        public int NewsItemId { get; set; }
        public NewsItem NewsItem { get; set; }

        public void Set(SavedNewsItem item)
        {
            User = item.User;
            NewsItem = item.NewsItem;
        }
    }
}
