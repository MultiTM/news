﻿using System;
using News.Data.Interfaces;

namespace News.Data.Entities
{
    public class Comment : IEntity
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
        
        public int UserId { get; set; }
        public User User { get; set; }

        public int NewsItemId { get; set; }
        public NewsItem NewsItem { get; set; }

        public void Set(Comment item)
        {
            Date = item.Date;
            Text = item.Text;
            NewsItem = item.NewsItem;
            User = item.User;
        }
    }
}
