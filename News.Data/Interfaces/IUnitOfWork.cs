﻿using System;
using System.Threading.Tasks;

namespace News.Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<T> Repository<T>() where T : class, IEntity;
        Task<int> Complete();
    }
}