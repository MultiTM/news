﻿namespace News.Data.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}