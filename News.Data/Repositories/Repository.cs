﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using News.Data.Interfaces;

namespace News.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        protected DbContext Context;

        public Repository(DbContext context)
        {
            Context = context;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public async Task<T> Get(int id)
        {
            return await Context.Set<T>().FindAsync(id);
        }

        public Task<List<T>> Get(Expression<Func<T, bool>> predicate, params Func<IQueryable<T>, IIncludableQueryable<T, object>>[] includes)
        {
            IQueryable<T> queryable = Context.Set<T>();
            foreach (var include in includes)
            {
                if (include != null)
                {
                    queryable = include(queryable);
                }
            }

            if (predicate != null)
            {
                queryable = queryable.Where(predicate);
            }

            return queryable.ToListAsync();
        }

        public async Task Add(T item)
        {
            await Context.Set<T>().AddAsync(item);
        }

        public async Task AddRange(IEnumerable<T> items)
        {
            await Context.Set<T>().AddRangeAsync(items);
        }

        public async Task Remove(T item)
        {
            item = await Get(item.Id);
            Context.Set<T>().Remove(item);
        }

        public async Task RemoveRange(IEnumerable<T> items)
        {
            List<T> attachedItems = new List<T>();
            var itemsList = items.ToList();
            foreach (var item in itemsList)
            {
                attachedItems.Add(await Get(item.Id));
            }

            Context.Set<T>().RemoveRange(attachedItems);
        }
    }
}