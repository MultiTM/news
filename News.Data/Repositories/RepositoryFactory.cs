﻿using Microsoft.EntityFrameworkCore;
using News.Data.Interfaces;

namespace News.Data.Repositories
{
    public class RepositoryFactory
    {
        public IRepository<T> CreateRepository<T>(DbContext context) where T : class, IEntity
        {
            return new Repository<T>(context);
        }
    }
}