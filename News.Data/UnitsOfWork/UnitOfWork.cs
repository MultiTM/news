﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using News.Data.EF;
using News.Data.Interfaces;
using News.Data.Repositories;

namespace News.Data.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly NewsContext _context;
        private readonly RepositoryFactory _factory;

        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();

        public UnitOfWork(NewsContext context, RepositoryFactory factory)
        {
            _context = context;
            _factory = factory;
        }

        public IRepository<T> Repository<T>() where T : class, IEntity
        {
            if (_repositories.Keys.Contains(typeof(T)))
            {
                return _repositories[typeof(T)] as IRepository<T>;
            }

            var repo = _factory.CreateRepository<T>(_context);
            _repositories.Add(typeof(T), repo);
            return repo;
        }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}