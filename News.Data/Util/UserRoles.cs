﻿namespace News.Data.Util
{
    public enum UserRoles
    {
        Guest = 1,
        User = 2,
        Admin = 3
    }
}
