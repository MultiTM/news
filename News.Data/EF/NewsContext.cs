﻿using Microsoft.EntityFrameworkCore;
using News.Data.Entities;

namespace News.Data.EF
{
    public class NewsContext : DbContext
    {
        public NewsContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Users
            modelBuilder.Entity<User>()
                .HasMany(u => u.Comments)
                .WithOne(c => c.User);

            modelBuilder.Entity<User>()
                .HasMany(u => u.Subscribes)
                .WithOne(s => s.User);

            modelBuilder.Entity<User>()
                .HasMany(u => u.SavedNews)
                .WithOne(s => s.User);

            //News
            modelBuilder.Entity<NewsItem>()
                .HasMany(n => n.Comments)
                .WithOne(c => c.NewsItem);

            modelBuilder.Entity<NewsItem>()
                .HasOne(n => n.RssSource);
        }

        public DbSet<NewsItem> News { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<RssSource> RssSources { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<SavedNewsItem> SavedNews { get; set; }
        public DbSet<Subscribe> Subscribes { get; set; }
        public DbSet<Comment> Comments { get; set; }

    }
}