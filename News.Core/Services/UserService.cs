﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using News.Core.DTO;
using News.Core.Interfaces;
using News.Core.Util;
using News.Data.Entities;
using News.Data.Interfaces;

namespace News.Core.Services
{
    public class UserService : Service<User, UserDto>, IUserService
    {
        private readonly AuthOptions _options;

        public UserService(IUnitOfWork uow, IMapper mapper, AuthOptions options) : base(uow, mapper)
        {
            _options = options;
        }

        public override async Task Update(UserDto itemDto)
        {
            var itemToUpdate = await Uow.Repository<User>().Get(itemDto.Id);
            var item = Mapper.Map<User>(itemDto);
            itemToUpdate.Set(item);
            await Uow.Complete();
        }

        public async Task<string> LogIn(UserDto user)
        {
            var identity = await GetIdentity(user.Username, user.Password);
            if (identity == null)
            {
                return null;
            }

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(_options.Lifetime)),
                signingCredentials: new SigningCredentials(_options.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }
        
        public async Task<bool> Register(UserDto userDto)
        {
            var matchUser = (await Uow.Repository<User>().Get(x => x.Username == userDto.Username)).FirstOrDefault();
            if (matchUser != null)
            {
                return false;
            }

            var user = Mapper.Map<User>(userDto);
            await Uow.Repository<User>().Add(user);
            await Uow.Complete();

            return true;
        }

        public async Task<UserDto> GetByUsername(string username)
        {
            var user = (await Uow.Repository<User>().Get(x => x.Username == username)).SingleOrDefault();
            return Mapper.Map<UserDto>(user);
        }

        private async Task<ClaimsIdentity> GetIdentity(string username, string password)
        {
            var users = await Uow.Repository<User>().GetAll();
            var user = users.FirstOrDefault(x => x.Username == username && x.Password == password);
            if (user != null)
            {
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, username), 
                    new Claim("Id", user.Id.ToString()), 
                    new Claim(user.Username, "Token", ClaimsIdentity.DefaultNameClaimType), 
                });
                return claimsIdentity;
            }
            
            return null;
        }

        public override async Task<IEnumerable<UserDto>> GetPage(PageInfo pageInfo)
        {
            var items = await GetItemsOnPage(pageInfo);
            if (items == null)
            {
                return null;
            }

            items = items.Skip((pageInfo.PageIndex - 1) * pageInfo.PageSize).Take(pageInfo.PageSize);

            return Mapper.Map<IEnumerable<UserDto>>(items);
        }

        protected override async Task<IEnumerable<User>> GetItemsOnPage(PageInfo pageInfo)
        {
            IEnumerable<User> items;
            if (pageInfo.FilterString != null)
            {
                items = await Uow.Repository<User>().Get(x => x.Username.Contains(pageInfo.FilterString));
            }
            else
            {
                items = await Uow.Repository<User>().GetAll();
            }

            return items.SortBy(pageInfo.SortField ?? "Username", pageInfo.SortAsc);
        }
    }
}
