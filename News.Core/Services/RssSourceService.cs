﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using News.Core.DTO;
using News.Core.Interfaces;
using News.Core.Util;
using News.Data.Entities;
using News.Data.Interfaces;

namespace News.Core.Services
{
    public class RssSourceService : Service<RssSource, RssSourceDto>, IRssSourceService
    {
        public RssSourceService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        { }

        public override async Task Update(RssSourceDto itemDto)
        {
            var itemToUpdate = await Uow.Repository<RssSource>().Get(itemDto.Id);
            var item = Mapper.Map<RssSource>(itemDto);
            itemToUpdate.Set(item);
            await Uow.Complete();
        }

        public override async Task<IEnumerable<RssSourceDto>> GetPage(PageInfo pageInfo)
        {
            var items = await GetItemsOnPage(pageInfo);
            if (items == null)
            {
                return null;
            }

            items = items.Skip((pageInfo.PageIndex - 1) * pageInfo.PageSize).Take(pageInfo.PageSize);

            return Mapper.Map<IEnumerable<RssSourceDto>>(items);
        }

        public override async Task<int> GetPageCount(PageInfo pageInfo)
        {
            var items = await GetItemsOnPage(pageInfo);
            if (items == null)
            {
                return 0;
            }

            var itemsCount = items.Count();

            return (int)Math.Ceiling((decimal)itemsCount / pageInfo.PageSize);
        }

        protected override async Task<IEnumerable<RssSource>> GetItemsOnPage(PageInfo pageInfo)
        {
            IEnumerable<RssSource> items;
            if (pageInfo.FilterString != null)
            {
                items = await Uow.Repository<RssSource>().Get(x => x.Name.Contains(pageInfo.FilterString));
            }
            else
            {
                items = await Uow.Repository<RssSource>().GetAll();
            }

            return items.SortBy(pageInfo.SortField ?? "Name", pageInfo.SortAsc);
        }
    }
}