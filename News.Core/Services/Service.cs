﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using News.Core.Interfaces;
using News.Core.Util;
using News.Data.Interfaces;

namespace News.Core.Services
{
    public class Service<TEntity, TDto> : IService<TEntity, TDto> where TEntity : class, IEntity
                                                                  where TDto : class, IDto
    {
        protected readonly IMapper Mapper;
        protected readonly IUnitOfWork Uow;

        public Service(IUnitOfWork uow, IMapper mapper)
        {
            Uow = uow;
            Mapper = mapper;
        }

        public virtual async Task<TDto> GetItem(int id)
        {
            var item = await Uow.Repository<TEntity>().Get(id);
            return Mapper.Map<TDto>(item);
        }

        public virtual async Task<IEnumerable<TDto>> GetAllAsync()
        {
            var items = await Uow.Repository<TEntity>().GetAll();
            return Mapper.Map<IEnumerable<TDto>>(items);
        }

        public virtual async Task Add(TDto itemDto)
        {
            var item = Mapper.Map<TEntity>(itemDto);
            await Uow.Repository<TEntity>().Add(item);
            await Uow.Complete();
        }

        public virtual async Task AddRange(IEnumerable<TDto> itemDtoList)
        {
            var items = Mapper.Map<IEnumerable<TEntity>>(itemDtoList);
            await Uow.Repository<TEntity>().AddRange(items);
            await Uow.Complete();
        }

        public virtual async Task Remove(TDto itemDto)
        {
            var item = Mapper.Map<TEntity>(itemDto);
            await Uow.Repository<TEntity>().Remove(item);
            await Uow.Complete();
        }

        public virtual async Task RemoveRange(IEnumerable<TDto> itemDtoList)
        {
            var items = Mapper.Map<IEnumerable<TEntity>>(itemDtoList);
            await Uow.Repository<TEntity>().RemoveRange(items);
            await Uow.Complete();
        }

        public virtual async Task Update(TDto itemDto)
        {
            throw new NotImplementedException();
        }

        public virtual Task<IEnumerable<TDto>> GetPage(PageInfo pageInfo)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<int> GetPageCount(PageInfo pageInfo)
        {
            var items = await GetItemsOnPage(pageInfo);
            if (items == null)
            {
                return 0;
            }

            var itemsCount = items.Count();

            return (int)Math.Ceiling((decimal)itemsCount / pageInfo.PageSize);
        }

        protected virtual Task<IEnumerable<TEntity>> GetItemsOnPage(PageInfo pageInfo)
        {
            throw new NotImplementedException();
        }
    }
}