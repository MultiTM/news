﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using News.Core.DTO;
using News.Core.Interfaces;
using News.Data.Entities;
using News.Data.Interfaces;

namespace News.Core.Services
{
    public class SubscribeService : Service<Subscribe, SubscribeDto>, ISubscribeService
    {
        public SubscribeService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override async Task Update(SubscribeDto itemDto)
        {
            var itemToUpdate = await Uow.Repository<Subscribe>().Get(itemDto.Id);
            var item = Mapper.Map<Subscribe>(itemDto);
            itemToUpdate.Set(item);
            await Uow.Complete();
        }

        public async Task<IEnumerable<SubscribeDto>> GetUserSubscribes(int userId)
        {
            var items = await Uow.Repository<Subscribe>().Get(s => s.User.Id == userId, include => include.Include(x => x.Category));
            var temp = Mapper.Map<IEnumerable<SubscribeDto>>(items);
            return temp;
        }

        public async Task<bool> Subscribe(SubscribeDto itemDto)
        {
            var user = await Uow.Repository<User>().Get(itemDto.UserId);
            var category = await Uow.Repository<Category>().Get(itemDto.CategoryId);
            if (user == null || category == null)
            {
                return false;
            }

            var item = (await Uow.Repository<Subscribe>().Get(x => x.CategoryId == itemDto.CategoryId && x.UserId == itemDto.UserId)).FirstOrDefault();
            if (item != null)
            {
                return true;
            }

            item = new Subscribe()
            {
                CategoryId = itemDto.CategoryId,
                UserId = itemDto.UserId
            };

            await Uow.Repository<Subscribe>().Add(item);
            await Uow.Complete();

            return true;
        }

        public async Task<bool> Unsubscribe(SubscribeDto itemDto)
        {
            var item = (await Uow.Repository<Subscribe>().Get(x => x.CategoryId == itemDto.CategoryId && x.UserId == itemDto.UserId)).FirstOrDefault();
            if (item == null)
            {
                return false;
            }

            await Uow.Repository<Subscribe>().Remove(item);
            await Uow.Complete();

            return true;
        }
    }
}
