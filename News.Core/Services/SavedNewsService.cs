﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using News.Core.DTO;
using News.Core.Interfaces;
using News.Data.Entities;
using News.Data.Interfaces;

namespace News.Core.Services
{
    public class SavedNewsService : Service<SavedNewsItem, SavedNewsItemDto>, ISavedNewsService
    {
        public SavedNewsService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override async Task Update(SavedNewsItemDto itemDto)
        {
            var itemToUpdate = await Uow.Repository<SavedNewsItem>().Get(itemDto.Id);
            var item = Mapper.Map<SavedNewsItem>(itemDto);
            itemToUpdate.Set(item);
            await Uow.Complete();
        }

        public async Task<IEnumerable<SavedNewsItemDto>> GetSavedByUser(int userId)
        {
            var items = await Uow.Repository<SavedNewsItem>().Get(s => s.User.Id == userId, include => include.Include(x => x.NewsItem));
            return Mapper.Map<IEnumerable<SavedNewsItemDto>>(items);
        }

        public async Task<bool> AddToSaved(SavedNewsItemDto itemDto)
        {
            var user = await Uow.Repository<User>().Get(itemDto.UserId);
            var newsItem = await Uow.Repository<NewsItem>().Get(itemDto.NewsItemId);
            if (user == null || newsItem == null)
            {
                return false;
            }

            var item = (await Uow.Repository<SavedNewsItem>().Get(x => x.NewsItemId == itemDto.NewsItemId && x.UserId == itemDto.UserId)).FirstOrDefault();
            if (item != null)
            {
                return true;
            }

            item = new SavedNewsItem()
            {
                NewsItemId = itemDto.NewsItemId,
                UserId = itemDto.UserId
            };

            await Uow.Repository<SavedNewsItem>().Add(item);
            await Uow.Complete();

            return true;
        }

        public async Task<bool> RemoveFromSaved(SavedNewsItemDto itemDto)
        {
            var item = (await Uow.Repository<SavedNewsItem>().Get(x => x.NewsItemId == itemDto.NewsItemId && x.UserId == itemDto.UserId)).FirstOrDefault();
            if (item == null)
            {
                return false;
            }

            await Uow.Repository<SavedNewsItem>().Remove(item);
            await Uow.Complete();

            return true;
        }
    }
}
