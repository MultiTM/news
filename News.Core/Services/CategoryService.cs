﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using News.Core.DTO;
using News.Core.Interfaces;
using News.Core.Util;
using News.Data.Entities;
using News.Data.Interfaces;

namespace News.Core.Services
{
    public class CategoryService : Service<Category, CategoryDto>, ICategoryService
    {
        public CategoryService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        { }

        public override async Task Update(CategoryDto itemDto)
        {
            var itemToUpdate = await Uow.Repository<Category>().Get(itemDto.Id);
            var item = Mapper.Map<Category>(itemDto);
            itemToUpdate.Set(item);
            await Uow.Complete();
        }

        public override async Task<IEnumerable<CategoryDto>> GetPage(PageInfo pageInfo)
        {
            var items = await GetItemsOnPage(pageInfo);
            if (items == null)
            {
                return null;
            }

            items = items.Skip((pageInfo.PageIndex - 1) * pageInfo.PageSize).Take(pageInfo.PageSize);

            return Mapper.Map<IEnumerable<CategoryDto>>(items);
        }

        protected override async Task<IEnumerable<Category>> GetItemsOnPage(PageInfo pageInfo)
        {
            IEnumerable<Category> items;
            if (pageInfo.FilterString != null)
            {
                items = await Uow.Repository<Category>().Get(x => x.Name.Contains(pageInfo.FilterString));
            }
            else
            {
                items = await Uow.Repository<Category>().GetAll();
            }

            return items.SortBy(pageInfo.SortField ?? "Name", pageInfo.SortAsc);
        }
    }
}