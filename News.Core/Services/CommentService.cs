﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using News.Core.DTO;
using News.Core.Interfaces;
using News.Data.Entities;
using News.Data.Interfaces;

namespace News.Core.Services
{
    public class CommentService : Service<Comment, CommentDto>, ICommentService
    {
        public CommentService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override async Task Update(CommentDto itemDto)
        {
            var itemToUpdate = await Uow.Repository<Comment>().Get(itemDto.Id);
            var item = Mapper.Map<Comment>(itemDto);
            itemToUpdate.Set(item);
            await Uow.Complete();
        }

        public async Task<IEnumerable<CommentDto>> GetCommentsByNewsItemId(int id)
        {
            var items = await Uow.Repository<Comment>().Get(c => c.NewsItem.Id == id, c => c.Include(com => com.NewsItem), c => c.Include(com => com.User));
            if (items == null)
            {
                return null;
            }

            return Mapper.Map<IEnumerable<CommentDto>>(items);
        }

        public async Task<IEnumerable<CommentDto>> GetCommentsByUserId(int id)
        {
            var items = await Uow.Repository<Comment>().Get(c => c.User.Id == id, c => c.Include(com => com.NewsItem), c => c.Include(com => com.User));
            if (items == null)
            {
                return null;
            }

            return Mapper.Map<IEnumerable<CommentDto>>(items);
        }

        public async Task<bool> AddComment(CommentDto comment)
        {
            comment.Date = DateTime.Now;

            var item = Mapper.Map<Comment>(comment);
            var user = await Uow.Repository<User>().Get(comment.UserId);
            var newsItem = await Uow.Repository<NewsItem>().Get(comment.NewsItemId);
            if (user == null || newsItem == null)
            {
                return false;
            }

            await Uow.Repository<Comment>().Add(item);
            await Uow.Complete();

            return true;
        }
    }
}
