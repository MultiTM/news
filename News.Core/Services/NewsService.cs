﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using News.Core.DTO;
using News.Core.Interfaces;
using News.Core.Util;
using News.Data.Entities;
using News.Data.Interfaces;

namespace News.Core.Services
{
    public class NewsService : Service<NewsItem, NewsItemDto>, INewsService
    {
        public NewsService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        { }

        public override async Task Update(NewsItemDto itemDto)
        {
            var itemToUpdate = await Uow.Repository<NewsItem>().Get(itemDto.Id);
            var item = Mapper.Map<NewsItem>(itemDto);
            itemToUpdate.Set(item);
            await Uow.Complete();
        }

        public async Task<NewsItemDto> GetLatestItemWithSourceId(int id)
        {
            var items = await Uow.Repository<NewsItem>().GetAll();
            if (items == null)
            {
                return null;
            }

            var item = items.Where(x => x.RssSourceId == id).OrderByDescending(x => x.Date).FirstOrDefault();
            return Mapper.Map<NewsItemDto>(item);
        }

        public async Task<IEnumerable<NewsItemDto>> GetPage(PageInfo pageInfo, int? userId = null)
        {
            IEnumerable<NewsItem> items;
            if (userId == null)
            {
                items = await GetItemsOnPage(pageInfo);
            }
            else
            {
                items = await GetItemsForPageOfUserSubs(pageInfo, (int)userId);
            }


            if (items == null)
            {
                return null;
            }

            items = items.Skip((pageInfo.PageIndex - 1) * pageInfo.PageSize).Take(pageInfo.PageSize);

            return Mapper.Map<IEnumerable<NewsItemDto>>(items);
        }

        protected override async Task<IEnumerable<NewsItem>> GetItemsOnPage(PageInfo pageInfo)
        {
            IEnumerable<NewsItem> items;
            if (pageInfo.FilterString != null)
            {
                items = await Uow.Repository<NewsItem>().Get(x => x.Title.Contains(pageInfo.FilterString), x => x.Include(n => n.Category));
            }
            else
            {
                items = await Uow.Repository<NewsItem>().Get(null, x => x.Include(n => n.Category));
            }

            if (pageInfo.CategoryId > 0)
            {
                items = items.Where(x => x.CategoryId == pageInfo.CategoryId);
            }

            return items.SortBy(pageInfo.SortField ?? "Title", pageInfo.SortAsc);
        }
        
        public async Task<IEnumerable<NewsItem>> GetItemsForPageOfUserSubs(PageInfo pageInfo, int userId)
        {
            var subscribedCategories = await Uow.Repository<Subscribe>().Get(x => x.UserId == userId, x => x.Include(n => n.Category));
            var subscribedCategoryIds = new List<int>();
            foreach (var subscribedCategory in subscribedCategories)
            {
                subscribedCategoryIds.Add(subscribedCategory.CategoryId);
            }

            IEnumerable<NewsItem> items = await Uow.Repository<NewsItem>().Get(x => subscribedCategoryIds.Contains(x.CategoryId));

            return items;
        }

        public async Task<int> GetPageCount(PageInfo pageInfo, int? userId = null)
        {
            IEnumerable<NewsItem> items;
            if (userId == null)
            {
                items = await GetItemsOnPage(pageInfo);
            }
            else
            {
                items = await GetItemsForPageOfUserSubs(pageInfo, (int)userId);
            }

            if (items == null)
            {
                return 0;
            }

            var itemsCount = items.Count();

            return (int)Math.Ceiling((decimal)itemsCount / pageInfo.PageSize);
        }
    }
}