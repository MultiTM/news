﻿using News.Core.Interfaces;

namespace News.Core.DTO
{
    public class RssSourceDto : IDto
    {
        public string Name { get; set; }
        public string RssLink { get; set; }
        public int Id { get; set; }
    }
}