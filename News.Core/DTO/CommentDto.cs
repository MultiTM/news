﻿using System;
using News.Core.Interfaces;

namespace News.Core.DTO
{
    public class CommentDto : IDto
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public string Author { get; set; }

        public int NewsItemId { get; set; }
        public string NewsItemTitle { get; set; }

        public DateTime Date { get; set; }
        public string Text { get; set; }
    }
}
