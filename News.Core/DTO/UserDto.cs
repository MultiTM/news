﻿using News.Core.Interfaces;

namespace News.Core.DTO
{
    public class UserDto : IDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
