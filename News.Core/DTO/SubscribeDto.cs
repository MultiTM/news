﻿using News.Core.Interfaces;

namespace News.Core.DTO
{
    public class SubscribeDto : IDto
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
