﻿using News.Core.Interfaces;

namespace News.Core.DTO
{
    public class SavedNewsItemDto : IDto
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int NewsItemId { get; set; }
        public string NewsItemTitle { get; set; }
    }
}
