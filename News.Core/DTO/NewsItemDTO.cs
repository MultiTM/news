﻿using System;
using News.Core.Interfaces;

namespace News.Core.DTO
{
    public class NewsItemDto : IDto
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public bool IsPublished { get; set; }
        public DateTime Date { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool IsExternal { get; set; }
        public string ExternalLink { get; set; }
        public int? RssSourceId { get; set; }
        public string RssSourceName { get; set; }
        public int Id { get; set; }
    }
}