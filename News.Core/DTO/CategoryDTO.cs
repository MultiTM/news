﻿using News.Core.Interfaces;

namespace News.Core.DTO
{
    public class CategoryDto : IDto
    {
        public string Name { get; set; }
        public int Id { get; set; }

    }
}