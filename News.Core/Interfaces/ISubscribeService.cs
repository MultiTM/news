﻿using System.Collections.Generic;
using System.Threading.Tasks;
using News.Core.DTO;
using News.Data.Entities;

namespace News.Core.Interfaces
{
    public interface ISubscribeService : IService<Subscribe, SubscribeDto>
    {
        Task<IEnumerable<SubscribeDto>> GetUserSubscribes(int userId);
        Task<bool> Subscribe(SubscribeDto subscribe);
        Task<bool> Unsubscribe(SubscribeDto subscribe);
    }

}
