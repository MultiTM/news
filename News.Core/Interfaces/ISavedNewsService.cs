﻿using System.Collections.Generic;
using System.Threading.Tasks;
using News.Core.DTO;
using News.Data.Entities;

namespace News.Core.Interfaces
{
    public interface ISavedNewsService : IService<SavedNewsItem, SavedNewsItemDto>
    {
        Task<IEnumerable<SavedNewsItemDto>> GetSavedByUser(int userId);
        Task<bool> AddToSaved(SavedNewsItemDto itemDto);
        Task<bool> RemoveFromSaved(SavedNewsItemDto itemDto);
    }
}
