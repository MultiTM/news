﻿using News.Core.DTO;
using News.Data.Entities;

namespace News.Core.Interfaces
{
    public interface ICategoryService : IService<Category, CategoryDto>
    { }
}