﻿using News.Core.DTO;
using News.Data.Entities;

namespace News.Core.Interfaces
{
    public interface IRssSourceService : IService<RssSource, RssSourceDto>
    { }
}