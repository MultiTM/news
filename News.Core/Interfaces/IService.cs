﻿using System.Collections.Generic;
using System.Threading.Tasks;
using News.Data.Interfaces;

namespace News.Core.Interfaces
{
    public interface IService<TEntity, TDto> where TEntity : class, IEntity
                                             where TDto : class, IDto
    {
        Task<TDto> GetItem(int id);
        Task<IEnumerable<TDto>> GetAllAsync();

        Task Update(TDto itemDto);

        Task Add(TDto itemDto);
        Task AddRange(IEnumerable<TDto> itemDtoList);

        Task Remove(TDto itemDto);
        Task RemoveRange(IEnumerable<TDto> itemDto);
    }
}