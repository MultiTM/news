﻿using System.Collections.Generic;
using System.Threading.Tasks;
using News.Core.DTO;
using News.Core.Util;
using News.Data.Entities;

namespace News.Core.Interfaces
{
    public interface INewsService : IService<NewsItem, NewsItemDto>
    {
        Task<NewsItemDto> GetLatestItemWithSourceId(int id);

        Task<IEnumerable<NewsItem>> GetItemsForPageOfUserSubs(PageInfo pageInfo, int userId);
    }
}