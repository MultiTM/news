﻿using System.Threading.Tasks;
using News.Core.DTO;
using News.Data.Entities;

namespace News.Core.Interfaces
{
    interface IUserService : IService<User, UserDto>
    {
        Task<string> LogIn(UserDto user);
        Task<bool> Register(UserDto user);

        Task<UserDto> GetByUsername(string username);
    }
}
