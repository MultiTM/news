﻿using System.Collections.Generic;
using System.Threading.Tasks;
using News.Core.DTO;
using News.Data.Entities;

namespace News.Core.Interfaces
{
    public interface ICommentService : IService<Comment, CommentDto>
    {
        Task<IEnumerable<CommentDto>> GetCommentsByNewsItemId(int id);
        Task<IEnumerable<CommentDto>> GetCommentsByUserId(int id);

        Task<bool> AddComment(CommentDto comment);
    }
}
