﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace News.Core.Util
{
    public class AuthOptions
    {
        public readonly string Issuer;
        public readonly string Audience;
        public readonly int Lifetime;

        private readonly string _key;

        public AuthOptions(string issuer, string audience, int lifetime, string key)
        {
            Issuer = issuer;
            Audience = audience;
            Lifetime = lifetime;
            _key = key;
        }

        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_key));
        }
    }
}
