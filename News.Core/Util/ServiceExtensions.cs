﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace News.Core.Util
{
    public static class ServiceExtensions
    {
        public static IEnumerable<TEntity> SortBy<TEntity>(this IEnumerable<TEntity> items, string sortField, bool sortAsc = true)
        {
            if (string.IsNullOrEmpty(sortField))
            {
                return items;
            }

            PropertyInfo prop = null;
            var props = typeof(TEntity).GetProperties();
            foreach (var propertyInfo in props)
            {
                if (string.Equals(propertyInfo.Name, sortField, StringComparison.CurrentCultureIgnoreCase))
                {
                    prop = propertyInfo;
                }
            }

            if (prop == null)
            {
                return null;
            }

            if (sortAsc)
            {
                return items.OrderBy(x => prop.GetValue(x, null));
            }
            else
            {
                return items.OrderByDescending(x => prop.GetValue(x, null));
            }
            
        }
    }
}
