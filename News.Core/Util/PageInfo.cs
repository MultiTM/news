﻿namespace News.Core.Util
{
    public class PageInfo
    {
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public string SortField { get; set; } 
        public bool SortAsc { get; set; } = true;
        public string FilterString { get; set; }
        public int CategoryId { get; set; } = 0;
    }
}