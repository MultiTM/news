﻿using System;
using Autofac;
using News.Core.Services;
using RssReader.Util;

namespace RssReader
{
    public class Program
    {
        private static void Main()
        {
            var container = AutofacConfig.ConfigureContainer();

            var rssReader = container.Resolve<BL.RssReader>();

            var rssSourceService = container.Resolve<RssSourceService>();
            var sourceList = rssSourceService.GetAllAsync().GetAwaiter().GetResult(); 

            foreach (var source in sourceList)
            {
                rssReader.DownloadAndSaveNews(source).GetAwaiter().GetResult();
            }
            
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}