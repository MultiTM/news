﻿using System;

namespace RssReader.Model
{
    public class RssNewsItem
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}