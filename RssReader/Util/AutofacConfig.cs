﻿using Autofac;
using Autofac.Extras.NLog;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using News.Core.Interfaces;
using News.Core.Services;
using News.Data.EF;
using News.Data.Interfaces;
using News.Data.Repositories;
using News.Data.UnitsOfWork;
using RssReader.BL;
using RssReader.Interfaces;

namespace RssReader.Util
{
    public class AutofacConfig
    {
        public static IContainer ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            builder.Register<DbContextOptions>(x => new DbContextOptionsBuilder().UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=Newsbase_2;Trusted_Connection=True").Options).AsSelf().SingleInstance();
            builder.RegisterType<NewsContext>().AsSelf();
            builder.RegisterType<RepositoryFactory>().AsSelf();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

            builder.RegisterType<NewsService>().As<INewsService>();
            builder.RegisterType<CategoryService>().As<ICategoryService>();
            builder.RegisterType<RssSourceService>().As<IRssSourceService>();

            builder.Register(context =>
            {
                var cfg = new MapperConfiguration(c => { c.AddProfile<AutomapperConfig>(); });
                return cfg.CreateMapper();
            }).As<IMapper>().SingleInstance();

            builder.RegisterModule<NLogModule>();
            
            builder.RegisterType<BL.RssReader>();
            builder.RegisterType<CategoryResolver>().As<ICategoryResolver>();
            builder.RegisterType<RssTransceiver>().As<IRssTransceiver>();
            
            return builder.Build();
        }
    }
}
