﻿using AutoMapper;
using News.Core.DTO;
using News.Data.Entities;

namespace RssReader.Util
{
    public class AutomapperConfig : Profile
    {
        public AutomapperConfig()
        {
            CreateMap<NewsItem, NewsItemDto>().ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name))
                                              .ForMember(dest => dest.RssSourceName, opt => opt.MapFrom(src => src.RssSource.Name))
                                              .ForMember(dest => dest.RssSourceId, opt => opt.MapFrom(src => src.RssSourceId));
            CreateMap<NewsItemDto, NewsItem>().ForMember(dest => dest.RssSourceId, opt => opt.MapFrom(src => src.RssSourceId));

            CreateMap<Category, CategoryDto>();
            CreateMap<CategoryDto, Category>();

            CreateMap<RssSource, RssSourceDto>();
            CreateMap<RssSourceDto, RssSource>();
        }
    }
}
