﻿using CodeHollow.FeedReader;

namespace RssReader.Util
{
    public static class FeedItemExtensions
    {
        public static bool HasAllProperties(this FeedItem newsItem)
        {
            return string.IsNullOrEmpty(newsItem.Title) &&
                   newsItem.PublishingDate != null &&
                   string.IsNullOrEmpty(newsItem.Link) &&
                   string.IsNullOrEmpty(newsItem.Description) &&
                   newsItem.Categories != null;
        }
    }
}
