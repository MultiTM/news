﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeHollow.FeedReader;
using News.Core.DTO;
using News.Core.Interfaces;
using NLog;
using RssReader.Interfaces;

namespace RssReader.BL
{
    public class CategoryResolver : ICategoryResolver
    {
        private readonly ICategoryService _categoryService;
        private readonly ILogger _logger;

        private Dictionary<string, int> _categoryCache;

        public CategoryResolver (ICategoryService categoryService)
        {
            _categoryService = categoryService;

            _logger = LogManager.GetCurrentClassLogger();
            
            _categoryCache = GetDictionaryFromCategoriesInDb().GetAwaiter().GetResult();
        }

        public int GetCategoryId(string categoryName) 
        {
            if (string.IsNullOrEmpty(categoryName))
            {
                throw new ApplicationException("Category name is null");
            }
            
            if (!_categoryCache.ContainsKey(categoryName))
            {
                throw new ApplicationException("No such category in database");
            }

            var category = _categoryCache[categoryName];
            return category;
        }

        public async Task AddAndUpdateCategoriesInCache(List<FeedItem> feedList)
        {
            _categoryCache = await GetDictionaryFromCategoriesInDb();

            foreach (var item in feedList)
            {
                var categoryName = item.Categories.FirstOrDefault();
                if (!string.IsNullOrEmpty(categoryName))
                {
                    if (!_categoryCache.ContainsKey(categoryName))
                    {
                        _categoryService.Add(new CategoryDto { Name = categoryName });

                        var newCategoryId = _categoryCache.Values.Max() + 1;
                        _categoryCache.Add(categoryName, newCategoryId);
                    }
                }
                else
                {
                    _logger.Error($"Category in feed item with title {item.Title} is null or empty");
                }
            }
            
            _categoryCache = await GetDictionaryFromCategoriesInDb();
        }

        private async Task<Dictionary<string, int>> GetDictionaryFromCategoriesInDb()
        {
            var categoryList = await _categoryService.GetAllAsync();
            var categoriesDictionary = new Dictionary<string, int>();

            if (categoryList != null)
            {
                foreach (var category in categoryList)
                {
                    categoriesDictionary.Add(category.Name, category.Id);
                }

                return categoriesDictionary;
            }
            throw new ApplicationException("Category list is null");
        }
    }
}
