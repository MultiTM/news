﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeHollow.FeedReader;
using News.Core.DTO;
using News.Core.Interfaces;
using NLog;
using RssReader.Interfaces;
using RssReader.Util;

namespace RssReader.BL
{
    public class RssReader
    {
        private readonly INewsService _newsService;
        private readonly ICategoryResolver _categoryResolver;
        private readonly IRssTransceiver _rssTransceiver;
        private readonly ILogger _logger;

        const int BatchSize = 100;

        public RssReader(INewsService newsService, ICategoryResolver categoryResolver, IRssTransceiver rssTransceiver)
        {
            _newsService = newsService;
            _categoryResolver = categoryResolver;
            _rssTransceiver = rssTransceiver;
            
            _logger = LogManager.GetCurrentClassLogger();
        }

        public async Task DownloadAndSaveNews(RssSourceDto rssSource)
        {
            _logger.Info($"Fetching from: {rssSource.Name}");

            var feedItems = await _rssTransceiver.GetFeed(rssSource);
            if (feedItems == null)
            {
                _logger.Error($"Empty feed list in source {rssSource.Name}");
            }

            var newsAddedCounter = 0;

            try
            {
                await _categoryResolver.AddAndUpdateCategoriesInCache(feedItems);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                _logger.Error($"Error while updating category list in database, source {rssSource.Name} was skipped");
                return;
            }

            var batchList = CreateBatchList(feedItems, rssSource);

            var newsBatchList = new List<List<NewsItemDto>>();

            foreach (var batch in batchList)
            {
                newsBatchList.Add(new List<NewsItemDto>());
                var currentNewsBatch = newsBatchList.Last();
                foreach (var feedItem in batch)
                {
                    var newsItem = MapFeedToNewsItemDto(feedItem, rssSource);
                    if (newsItem == null)
                    {
                        _logger.Error($"Error occured while mapping news from source {rssSource.Name}, that news was skipped");
                        continue;
                    }
                    currentNewsBatch.Add(newsItem);
                }
            }

            foreach (var newsBatch in newsBatchList)
            {
                _newsService.AddRange(newsBatch);

                if (newsBatch.Count > 0)
                {
                    _logger.Info("Batch saved");
                }

                newsAddedCounter += newsBatch.Count;
            }

            _logger.Info($"Total news added: {newsAddedCounter}");
        }

        private List<List<FeedItem>> CreateBatchList(List<FeedItem> feedList, RssSourceDto source)
        {
            var batchList = new List<List<FeedItem>> {new List<FeedItem>()};
            var currentBatch = batchList.Last();

            foreach (var feedItem in feedList)
            {
                if (currentBatch.Count >= BatchSize)
                {
                    batchList.Add(new List<FeedItem>());
                    currentBatch = batchList.Last();
                }

                if (feedItem == null)
                {
                    _logger.Warn($"Null feeditem in source {source.Name}");
                }
                currentBatch.Add(feedItem);
            }

            return batchList;
        }

        private NewsItemDto MapFeedToNewsItemDto(FeedItem feedItem, RssSourceDto source)
        {
            if (feedItem.Categories != null)
            {
                var categoryName = feedItem.Categories.FirstOrDefault();
                if (!string.IsNullOrEmpty(categoryName) || feedItem.HasAllProperties())
                {
                    int categoryId;
                    try
                    {
                        categoryId = _categoryResolver.GetCategoryId(categoryName);
                    }
                    catch (Exception ex)
                    {
                        _logger.Warn(ex);
                        _logger.Warn($"Erroe occured while getting category id in source {source.Name}, news with title {feedItem.Title} was skipped");
                        return null;
                    }

                    return new NewsItemDto
                    {
                        Title = feedItem.Title,
                        Text = feedItem.Description,
                        // ReSharper disable once PossibleInvalidOperationException
                        Date = feedItem.PublishingDate.Value,
                        IsPublished = true,
                        IsExternal = true,
                        ExternalLink = feedItem.Link,
                        CategoryId = categoryId,
                        RssSourceId = source.Id
                    };
                }
            }

            return null;
        }
    }
}
