﻿using System.ComponentModel.DataAnnotations;

namespace News.Areas.AdminPanel.Models
{
    public class AccountInfoViewModel
    {
        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }
    }
}