﻿using System.ComponentModel.DataAnnotations;

namespace News.Areas.AdminPanel.Models
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Category Name is required")]
        [MinLength(3, ErrorMessage = "Name must be 3 characters or more")]
        public string Name { get; set; }
    }
}