﻿namespace News.Areas.AdminPanel.Models
{
    public class NewsItemDeleteViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}