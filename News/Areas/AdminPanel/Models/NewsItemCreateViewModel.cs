﻿using System.ComponentModel.DataAnnotations;

namespace News.Areas.AdminPanel.Models
{
    public class NewsItemCreateViewModel
    {
        [Required(ErrorMessage = "Title is required")]
        [StringLength(120, ErrorMessage = "Title must be 3-120 characters", MinimumLength = 3)]
        public string Title { get; set; }
        [Required(ErrorMessage = "Text is required")]
        [MinLength(3, ErrorMessage = "Text must be 3 characters or more")]
        public string Text { get; set; }
        public bool IsPublished { get; set; }
        public int CategoryId { get; set; }
    }
}