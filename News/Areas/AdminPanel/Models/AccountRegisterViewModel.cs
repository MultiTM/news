﻿using System.ComponentModel.DataAnnotations;

namespace News.Areas.AdminPanel.Models
{
    public class AccountRegisterViewModel : AccountLoginViewModel
    {
        [Required(ErrorMessage = "Password confirmation is required")]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

    }
}