﻿namespace News.Areas.AdminPanel.Models
{
    public class NewsItemEditViewModel : NewsItemCreateViewModel
    {
        public string Id { get; set; }
        public string ExternalLink { get; set; }
    }
}