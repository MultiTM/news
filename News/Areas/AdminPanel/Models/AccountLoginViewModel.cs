﻿using System.ComponentModel.DataAnnotations;

namespace News.Areas.AdminPanel.Models
{
    public class AccountLoginViewModel : AccountInfoViewModel
    {
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
}