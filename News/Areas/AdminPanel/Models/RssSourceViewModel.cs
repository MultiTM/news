﻿namespace News.Areas.AdminPanel.Models
{
    public class RssSourceViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string RssLink { get; set; }
    }
}