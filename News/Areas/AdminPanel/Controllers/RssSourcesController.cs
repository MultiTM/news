﻿using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using News.Areas.AdminPanel.Models;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.Models;

namespace News.Areas.AdminPanel.Controllers
{
    public class RssSourcesController : Controller
    {
        private readonly IService<RssSourceDto> _rssSourceService;
        private readonly IMapper _mapper;

        public RssSourcesController(IService<RssSourceDto> rssSourcesService, IMapper mapper)
        {
            _rssSourceService = rssSourcesService;
            _mapper = mapper;
        }

        public async Task<ActionResult> Index(string filter, int page = 1, int pageSize = 10, string sortingField = "Name", bool isSortingAscending = true)
        {
            var pageInfo = new PageInfoDto
            {
                CurrentPage = page,
                PageSize = pageSize,
                SortingField = sortingField,
                IsSortingAscending = isSortingAscending,
                Filter = filter
            };
            var rssSourcesPage = await _rssSourceService.GetPageAsync(pageInfo);

            return View(_mapper.Map<PageViewModel<RssSourceViewModel>>(rssSourcesPage));
        }

        public ActionResult Create()
        {
            return View(new RssSourceViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Create(RssSourceViewModel createdRssSourceItem)
        {
            if (ModelState.IsValid)
            {
                var createdItemDto = _mapper.Map<RssSourceDto>(createdRssSourceItem);
                await _rssSourceService.Add(createdItemDto);
                return RedirectToAction("Index", new { area = "AdminPanel", controller = "RssSources" });
            }

            return View();
        }

        public async Task<ActionResult> Edit(int id)
        {
            var editItemDto = await _rssSourceService.GetItem(id);
            if (editItemDto == null)
            {
                return HttpNotFound();
            }

            return View(_mapper.Map<RssSourceViewModel>(editItemDto));
        }

        [HttpPost]
        public async Task<ActionResult> Edit(RssSourceViewModel editedRssSourceItem)
        {
            if (ModelState.IsValid)
            {
                await _rssSourceService.Edit(_mapper.Map<RssSourceDto>(editedRssSourceItem));
                return RedirectToAction("Index", new { area = "AdminPanel", controller = "RssSources" });
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var deletedRssSourceItem = _mapper.Map<RssSourceViewModel>(await _rssSourceService.GetItem(id));
            if (deletedRssSourceItem == null)
            {
                return HttpNotFound();
            }

            await _rssSourceService.Delete(_mapper.Map<RssSourceDto>(deletedRssSourceItem));
            return RedirectToAction("Index", new { area = "AdminPanel", controller = "RssSources" });
        }
    }
}