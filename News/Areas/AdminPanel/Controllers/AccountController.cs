﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using News.Areas.AdminPanel.Models;
using News.DAL.Entities;
using News.DAL.Identity;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace News.Areas.AdminPanel.Controllers
{
    
    public class AccountController : Controller
    {
        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

        private IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [Authorize]
        public ActionResult Index()
        {
            var user = new AccountInfoViewModel()
            {
                Username = AuthManager.User.Identity.Name
            };
            return View(user);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(AccountRegisterViewModel registerModel)
        {
            if (ModelState.IsValid)
            {
                var user = new AppUser()
                {
                    UserName = registerModel.Username,
                    Email = registerModel.Email
                };
                var result = await UserManager.CreateAsync(user, registerModel.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.invalid = "Something gone wrong. Registration failed.";
                    return View();
                }
            }
            return View();
        }

        
        public ActionResult Logout()
        {
            AuthManager.SignOut();
            return RedirectToAction("Index");
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(AccountLoginViewModel loginModel)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(loginModel.Username, loginModel.Password);
                if (user != null)
                {
                    var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthManager.SignOut();
                    AuthManager.SignIn(new AuthenticationProperties { IsPersistent = false }, identity);
                    return RedirectToAction("Index");
                }
                ViewBag.invalid = "Invalid login or password";
                return View();
            }

            return View();
        }
    }
}