﻿using AutoMapper;
using News.Areas.AdminPanel.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.ExceptionFilters;
using News.Models;

namespace News.Areas.AdminPanel.Controllers
{
    [Authorize]
    public class NewsController : Controller
    {
        private readonly INewsService _newsService;

        private readonly IService<CategoryDto> _categoryService;

        private readonly IMapper _mapper;

        public NewsController(INewsService newsService, IService<CategoryDto> categoryService, IMapper mapper)
        {
            _newsService = newsService;
            _categoryService = categoryService;
            _mapper = mapper;
        }

        public async Task<ActionResult> Index(string filter = null, int page = 1, int pageSize = 10, string sortingField = "Date", bool isSortingAscending = false, string category = null)
        {
            var pageInfo = new PageInfoDto
            {
                CurrentPage = page,
                PageSize = pageSize,
                SortingField = sortingField,
                IsSortingAscending = isSortingAscending,
                Filter = filter,
                Category = category
            };
            var newsPage = await _newsService.GetPageAsync(pageInfo);

            ViewBag.Categories = _mapper.Map<IEnumerable<CategoryViewModel>>(await _categoryService.GetAllItemsAsync());

            return View(_mapper.Map<PageViewModel<NewsItemViewModel>>(newsPage));
        }


        public async Task<ActionResult> Create()
        {
            ViewBag.Categories = _mapper.Map<IEnumerable<CategoryViewModel>>(await _categoryService.GetAllItemsAsync());
            return View("Create", new NewsItemCreateViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Create(NewsItemCreateViewModel createdNewsItem)
        {
            if (ModelState.IsValid)
            {
                var createdItemDto = _mapper.Map<NewsItemDto>(createdNewsItem);
                await _newsService.Add(createdItemDto);
                return RedirectToAction("Index", new { area = "AdminPanel", controller = "News" });
            }

            ViewBag.Categories = _mapper.Map<IEnumerable<CategoryViewModel>>(await _categoryService.GetAllItemsAsync());
            return View("Create");
        }

        public async Task<ActionResult> Edit(int id)
        {
            var editItemDto = await _newsService.GetItem(id);
            if (editItemDto == null)
            {
                return HttpNotFound();
            }

            ViewBag.Categories = _mapper.Map<IEnumerable<CategoryViewModel>>(await _categoryService.GetAllItemsAsync());

            return View("Edit", _mapper.Map<NewsItemEditViewModel>(editItemDto));
        }

        [HttpPost]
        [ItemEditExceptionFilter]
        public async Task<ActionResult> Edit(NewsItemEditViewModel editedNewsItem)
        {
            if (ModelState.IsValid)
            {
                await _newsService.Edit(_mapper.Map<NewsItemDto>(editedNewsItem));
                return RedirectToAction("Index", new { area = "AdminPanel", controller = "News" });
            }

            ViewBag.Categories = _mapper.Map<IEnumerable<CategoryViewModel>>(await _categoryService.GetAllItemsAsync());
            return View("Edit");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var deletedNewsItem = _mapper.Map<NewsItemDeleteViewModel>(await _newsService.GetItem(id));
            if (deletedNewsItem == null)
            {
                return HttpNotFound();
            }
            
            await _newsService.Delete(_mapper.Map<NewsItemDto>(deletedNewsItem));
            return RedirectToAction("Index", new { area = "AdminPanel", controller = "News" });
        }
    }
}