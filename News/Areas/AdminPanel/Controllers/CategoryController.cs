﻿using AutoMapper;
using System.Threading.Tasks;
using System.Web.Mvc;
using News.Areas.AdminPanel.Models;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.Models;

namespace News.Areas.AdminPanel.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IService<CategoryDto> _categoryService;
        private readonly IMapper _mapper;

        public CategoryController(IService<CategoryDto> categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }
        public async Task<ActionResult> Index(string filter, int page = 1, int pageSize = 10, string sortingField = "Name", bool isSortingAscending = true)
        {
            var pageInfo = new PageInfoDto {
                CurrentPage = page,
                PageSize = pageSize,
                SortingField = sortingField,
                IsSortingAscending = isSortingAscending,
                Filter = filter
            };
            var categoryPage = await _categoryService.GetPageAsync(pageInfo);

            return View(_mapper.Map<PageViewModel<CategoryViewModel>>(categoryPage));
        }

        public ActionResult Create()
        {
            return View(new CategoryViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Create(CategoryViewModel createdCategoryItem)
        {
            if (ModelState.IsValid)
            {
                var createdItemDto = _mapper.Map<CategoryDto>(createdCategoryItem);
                await _categoryService.Add(createdItemDto);
                return RedirectToAction("Index", new { area = "AdminPanel", controller = "Category" });
            }

            return View();
        }

        public async Task<ActionResult> Edit(int id)
        {
            var editItemDto = await _categoryService.GetItem(id);
            if (editItemDto == null)
            {
                return HttpNotFound();
            }

            return View(_mapper.Map<CategoryViewModel>(editItemDto));
        }

        [HttpPost]
        public async Task<ActionResult> Edit(CategoryViewModel editedCategoryItem)
        {
            if (ModelState.IsValid)
            {
                await _categoryService.Edit(_mapper.Map<CategoryDto>(editedCategoryItem));
                return RedirectToAction("Index", new { area = "AdminPanel", controller = "Category" });
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var deletedCategoryItem = _mapper.Map<CategoryViewModel>(await _categoryService.GetItem(id));
            if (deletedCategoryItem == null)
            {
                return HttpNotFound();
            }

            await _categoryService.Delete(_mapper.Map<CategoryDto>(deletedCategoryItem));
            return RedirectToAction("Index", new { area = "AdminPanel", controller = "Category" });
        }
    }
}