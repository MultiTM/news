﻿using System.Web.Mvc;
using News.DAL.Exceptions;

namespace News.ExceptionFilters
{
    public class ItemEditExceptionFilter : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && filterContext.Exception is ItemEditException)
            {
                filterContext.Result = new HttpNotFoundResult();
                filterContext.ExceptionHandled = true;
            }
        }
    }
}