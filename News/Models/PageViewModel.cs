﻿using System.Collections.Generic;
using News.BLL.DTO;

namespace News.Models
{
    public class PageViewModel<T> where T : class
    {
        public PageInfoDto PageInfo { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}