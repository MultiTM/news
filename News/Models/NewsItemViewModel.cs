﻿using System;

namespace News.Models
{
    public class NewsItemViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public bool IsPublished { get; set; }
        public DateTime Date { get; set; }
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public bool IsExternal { get; set; }
        public string ExternalLink { get; set; }
        public string SourceName { get; set; }
    }
}