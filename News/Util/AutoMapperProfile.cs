﻿using AutoMapper;
using News.Areas.AdminPanel.Models;
using News.BLL.DTO;
using News.DAL.Entities;
using News.Models;

namespace News.Util
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<NewsItemViewModel, NewsItemViewModel>();
            CreateMap<NewsItemViewModel, NewsItemViewModel> ();

            CreateMap<NewsItemCreateViewModel, NewsItemDto>();
            CreateMap<NewsItemDto, NewsItemCreateViewModel>();

            CreateMap<NewsItemDto, NewsItemEditViewModel>();
            CreateMap<NewsItemEditViewModel, NewsItemDto>();

            CreateMap<NewsItemDeleteViewModel, NewsItemDto>();
            CreateMap<NewsItemDto, NewsItemDeleteViewModel>();

            CreateMap<NewsItem, NewsItemDto>().ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name))
                                              .ForMember(dest => dest.SourceName, opt => opt.MapFrom(src => src.Source.Name))
                                              .ForMember(dest => dest.SourceId, opt => opt.MapFrom(src => src.RssSourceId));
            CreateMap<NewsItemDto, NewsItem>().ForMember(dest => dest.RssSourceId, opt => opt.MapFrom(src => src.SourceId)); 

            CreateMap<Category, CategoryDto>();
            CreateMap<CategoryDto, Category>();

            CreateMap<CategoryDto, CategoryViewModel>();
            CreateMap<CategoryViewModel, CategoryDto>();

            CreateMap<RssSource, RssSourceDto>();
            CreateMap<RssSourceDto, RssSource>();

            CreateMap<RssSourceDto, RssSourceViewModel>();
            CreateMap<RssSourceViewModel, RssSourceDto>();

            CreateMap<PageDto<NewsItemDto>, PageViewModel<NewsItemDto>>();
            CreateMap<PageViewModel<NewsItemDto>, PageDto<NewsItemDto>>();

            CreateMap<PageDto<CategoryDto>, PageViewModel<CategoryDto>>();
            CreateMap<PageViewModel<CategoryDto>, PageDto<CategoryDto>>();

            CreateMap<PageDto<RssSourceDto>, PageViewModel<RssSourceDto>>();
            CreateMap<PageViewModel<RssSourceDto>, PageDto<RssSourceDto>>();
        }
    }
}