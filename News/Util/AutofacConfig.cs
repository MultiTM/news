﻿using System.Data.Entity;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using News.Areas.AdminPanel.Controllers;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.BLL.Services;
using News.Controllers;
using News.DAL.EF;
using News.DAL.Entities;
using News.DAL.Identity;
using News.DAL.Interfaces;
using News.DAL.Repositories;

namespace News.Util
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<HomeController>().InstancePerRequest();
            builder.RegisterType<NewsService>().As<INewsService>().InstancePerRequest();
            builder.RegisterType<CategoryService>().As<IService<CategoryDto>>().InstancePerRequest();
            builder.RegisterType<RssSourceService>().As<IService<RssSourceDto>>().InstancePerRequest();
            builder.RegisterType<EfUnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            builder.RegisterType<NewsController>();
            builder.RegisterType<CategoryController>();
            builder.RegisterType<RssSourcesController>();
            builder.RegisterType<AppUserManager>();
            builder.RegisterType<UserStore<AppUser>>().As<IUserStore<AppUser>>();
            builder.RegisterType<NewsContext>().As<DbContext>().AsSelf().InstancePerRequest();

            builder.Register(context =>
            {
                var cfg = new MapperConfiguration(c => { c.AddProfile<AutoMapperProfile>(); });
                return cfg.CreateMapper();
            }).As<IMapper>().SingleInstance();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}