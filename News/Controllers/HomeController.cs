﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using News.Areas.AdminPanel.Models;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.Models;

namespace News.Controllers
{
    public class HomeController : Controller
    {
        private readonly INewsService _newsService;

        private readonly IService<CategoryDto> _categoryService;

        private readonly IMapper _mapper;

        public HomeController(INewsService newsService, IService<CategoryDto> categoryService, IMapper mapper)
        {
            _newsService = newsService;
            _categoryService = categoryService;
            _mapper = mapper;
        }

        public async Task<ActionResult> Index(int page = 1, int pageSize = 15, string category = null)
        {
            var pageInfo = new PageInfoDto
            {
                CurrentPage = page,
                PageSize = pageSize,
                SortingField = "Date",
                Category = category
            };
            var newsPage = await _newsService.GetPageAsync(pageInfo, true);

            var categoryList = await _categoryService.GetAllItemsAsync();

            ViewBag.Categories = _mapper.Map<IEnumerable<CategoryViewModel>>(categoryList);

            return View(_mapper.Map<PageViewModel<NewsItemViewModel>>(newsPage));
        }

        public async Task<ActionResult> Detail(int id)
        {
            var newsItem = await _newsService.GetItem(id);
            if (newsItem == null)
            {
                return HttpNotFound();
            }

            var newsItemVm = _mapper.Map<NewsItemViewModel>(newsItem);
            return View(newsItemVm);
        }
    }
}