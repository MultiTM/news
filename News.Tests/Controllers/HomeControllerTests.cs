﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Moq;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using News.Util;
using System.Threading.Tasks;
using News.Areas.AdminPanel.Models;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.Controllers;
using News.Models;

namespace News.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTests
    {
        private IMapper _mapper;

        private readonly List<NewsItemDto> _testNewsList = new List<NewsItemDto>();
        private readonly List<CategoryDto> _testCategoryList = new List<CategoryDto>();


        [TestInitialize]
        public void Init()
        {
            _mapper = new MapperConfiguration(c => { c.AddProfile<AutoMapperProfile>(); }).CreateMapper();

            _testNewsList.Add(new NewsItemDto()
            {
                Title = "Title_1",
                CategoryId = 1,
                CategoryName = "Cat_1",
                IsPublished = true,
                Text = "Text_1",
                Date = DateTime.Now,
                Id = 1
            });
            _testNewsList.Add(new NewsItemDto()
            {
                Title = "Title_2",
                CategoryId = 1,
                CategoryName = "Cat_1",
                IsPublished = true,
                Text = "Text_2",
                Date = DateTime.Now,
                Id = 2
            });
            _testNewsList.Add(new NewsItemDto()
            {
                Title = "Title_3",
                CategoryId = 2,
                CategoryName = "Cat_2",
                IsPublished = true,
                Text = "Text_3",
                Date = DateTime.Now,
                Id = 3
            });


            _testCategoryList.Add(new CategoryDto()
            {
                Id = 1,
                Name = "Cat_1"
            });
            _testCategoryList.Add(new CategoryDto()
            {
                Id = 2,
                Name = "Cat_2"
            });
        }


        [TestMethod]
        public async Task Index_NullArgs_ViewAndViewBagNotNull()
        {
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.GetPageAsync(It.IsAny<PageInfoDto>(), It.IsAny<bool>()))
                .ReturnsAsync(new PageDto<NewsItemDto>() {Items = _testNewsList});

            categoryServiceMock.Setup(n => n.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            var controller = new HomeController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            var result = await controller.Index() as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ViewBag.Categories as IEnumerable<CategoryViewModel>);
        }

        [TestMethod]
        public async Task Index_CategoryNamePassed_ModelItemsCategoriesMatchCategoryName()
        {
            //Arrange
            var categoryName = "Cat_1";

            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n =>
                    n.GetPageAsync(It.Is<PageInfoDto>(x => x.Category == categoryName), It.IsAny<bool>()))
                .ReturnsAsync(new PageDto<NewsItemDto>()
                {
                    Items = _testNewsList.Where(n => n.CategoryName == categoryName),
                    PageInfo = new PageInfoDto(_testNewsList.Count(), 1, 10, null, false, null, categoryName)
                });

            categoryServiceMock.Setup(n => n.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            //Act
            var controller = new HomeController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);

            var result = await controller.Index(category: categoryName);

            //Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            Assert.IsNotNull(viewResult.Model);

            var page = (PageViewModel<NewsItemViewModel>) viewResult.Model;
            var news = page.Items;
            var newsItems = news.ToList();
            Assert.AreEqual(newsItems.Count(), _testNewsList.Count(n => n.CategoryName == categoryName));

            foreach (var newsItem in newsItems)
            {
                Assert.AreEqual(categoryName, newsItem.CategoryName);
            }
        }

        [TestMethod]
        public async Task Index_CategoryNameNull_ModelHasAllItems()
        {
            //Arrange
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.GetPageAsync(It.Is<PageInfoDto>(x => x.Category == null), It.IsAny<bool>()))
                .ReturnsAsync(new PageDto<NewsItemDto>()
                {
                    Items = _testNewsList,
                    PageInfo = new PageInfoDto(_testNewsList.Count(), 1, 10, null, false, null, null)
                });

            categoryServiceMock.Setup(n => n.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            //Act
            var controller = new HomeController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);

            var result = await controller.Index();

            //Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            Assert.IsNotNull(viewResult.Model);

            var page = (PageViewModel<NewsItemViewModel>) viewResult.Model;
            var news = page.Items;
            Assert.AreEqual(news.Count(), _testNewsList.Count());
        }

        [TestMethod]
        public async Task Index_CategoryNameForCategoryIsntExist_ModelHasNoItems()
        {
            //Arrange
            var categoryName = "Cat_3";

            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n =>
                    n.GetPageAsync(It.Is<PageInfoDto>(x => x.Category == categoryName), It.IsAny<bool>()))
                .ReturnsAsync(new PageDto<NewsItemDto>()
                {
                    Items = _testNewsList.Where(n => n.CategoryName == categoryName),
                    PageInfo = new PageInfoDto(_testNewsList.Count(), 1, 10, null, false, null, categoryName)
                });

            categoryServiceMock.Setup(n => n.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            //Act
            var controller = new HomeController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);

            var result = await controller.Index(category: categoryName);

            //Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            Assert.IsNotNull(viewResult.Model);

            var page = (PageViewModel<NewsItemViewModel>) viewResult.Model;
            var news = page.Items;
            Assert.IsNotNull(news);
            Assert.AreEqual(news.Count(), 0);
        }

        [TestMethod]
        public async Task Detail_IdNull_HttpNotFound()
        {
            //Arrange
            int id = 123;

            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.GetItem(id)).ReturnsAsync((NewsItemDto) null);

            //Act
            var controller = new HomeController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);

            var result = await controller.Detail(id);

            //Assert
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
        }

        [TestMethod]
        public async Task Detail_IdIsOne_ViewResultModelNewsItemWithIdOne()
        {
            //Arrange
            int id = 1;

            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.GetItem(id)).ReturnsAsync(new NewsItemDto {Id = id});

            //Act
            var controller = new HomeController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);

            var result = await controller.Detail(id);

            //Assert
            var viewResult = (ViewResult) result;
            Assert.IsNotNull(viewResult.Model);

            var newsItem = _mapper.Map<NewsItemViewModel>(viewResult.Model);

            Assert.AreEqual(id, newsItem.Id);
        }
    }
}