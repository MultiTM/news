﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using News.Areas.AdminPanel.Controllers;
using News.Areas.AdminPanel.Models;
using News.BLL.DTO;
using News.BLL.Interfaces;
using News.DAL.Exceptions;
using News.Util;

namespace News.Tests.Areas.AdminPanel.Controllers
{

    [TestClass]
    public class NewsControllerTests
    {
        private IMapper _mapper;

        private readonly List<CategoryDto> _testCategoryList = new List<CategoryDto>();

        [TestInitialize]
        public void Init()
        {
            _mapper = new MapperConfiguration(c => { c.AddProfile<AutoMapperProfile>(); }).CreateMapper();

            _testCategoryList.Add(new CategoryDto()
            {
                Id = 1,
                Name = "Cat_1"
            });
            _testCategoryList.Add(new CategoryDto()
            {
                Id = 2,
                Name = "Cat_2"
            });
        }

        [TestMethod]
        public async Task CreateGet_NullArgs_ViewBagCategoriesNotNull()
        {
            //Arrange
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            categoryServiceMock.Setup(c => c.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            var result = await controller.Create() as ViewResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Create", result.ViewName);
            Assert.IsNotNull(result.ViewBag.Categories as IEnumerable<CategoryViewModel>);
        }

        [TestMethod]
        public async Task CreatePost_IF_ModelIsValid_SHOULD_AddInvoked()
        {
            //Arrange
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.Add(It.IsNotNull<NewsItemDto>())).Returns(Task.CompletedTask).Verifiable();

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            await controller.Create(new NewsItemCreateViewModel());

            //Assert
            newsServiceMock.Verify();
        }

        [TestMethod]
        public async Task CreatePost_ModelIsInvalid_ReturnView()
        {
            //Arrange
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();
            
            categoryServiceMock.Setup(c => c.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            controller.ModelState.AddModelError("Title", "Too short");
            var result = await controller.Create(new NewsItemCreateViewModel()) as ViewResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ViewName, "Create");
        }

        [TestMethod]
        public async Task EditGet_NullId_ReturnHttpNotFound()
        {
            var itemId = 654;

            //Arrange
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.GetItem(itemId)).ReturnsAsync((NewsItemDto)null);

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            var result = await controller.Edit(itemId);

            //Assert
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
        }

        [TestMethod]
        public async Task EditGet_IdPassed_ReturnView()
        {
            int id = 1;

            //Arrange
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.GetItem(id)).ReturnsAsync(new NewsItemDto());
            categoryServiceMock.Setup(c => c.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            var result = await controller.Edit(id) as ViewResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ViewName, "Edit");
            Assert.IsNotNull(result.ViewBag.Categories as IEnumerable<CategoryViewModel>);
            Assert.IsNotNull(result.Model as NewsItemEditViewModel);
        }

        [TestMethod]
        public async Task EditPost_ModelIsValid_EditInvokedReturnRedirect()
        {
            //Arrange
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.Edit(It.IsNotNull<NewsItemDto>())).Returns(Task.CompletedTask).Verifiable();

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            var result = await controller.Edit(new NewsItemEditViewModel()) as RedirectToRouteResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", (string)result.RouteValues["action"]);
            Assert.AreEqual("News", (string)result.RouteValues["controller"]);
            Assert.AreEqual("AdminPanel", (string)result.RouteValues["area"]);
            newsServiceMock.Verify();
        }

        [TestMethod]
        public async Task EditPost_ModelIsInvalid_ReturnView()
        {
            //Arrange
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            categoryServiceMock.Setup(c => c.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            controller.ModelState.AddModelError("Title", "Too short");
            var result = await controller.Edit(new NewsItemEditViewModel()) as ViewResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ViewName, "Edit");
        }

        [TestMethod]
        [ExpectedException(typeof(ItemEditException))]
        public async Task EditPost_ModelIsValidEditError_ReturnHttpNotFound()
        {
            //Arrange
            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.Edit(It.IsNotNull<NewsItemDto>())).Throws(new ItemEditException(""));
            categoryServiceMock.Setup(c => c.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            await controller.Edit(new NewsItemEditViewModel());

            //Assert
        }

        [TestMethod]
        public async Task Delete_NullId_ReturnHttpNotFound()
        {
            //Arrange
            int id = 1;

            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.GetItem(id)).ReturnsAsync((NewsItemDto)null);

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            var result = await controller.Delete(id);

            //Assert
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
        }

        [TestMethod]
        public async Task Delete_IdPassed_ReturnRedirect()
        {
            //Arrange
            int id = 1;

            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.GetItem(It.IsAny<int>())).ReturnsAsync(new NewsItemDto());
            newsServiceMock.Setup(n => n.Delete(It.IsAny<NewsItemDto>())).Returns(Task.CompletedTask).Verifiable();
            categoryServiceMock.Setup(c => c.GetAllItemsAsync()).ReturnsAsync(_testCategoryList);

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            var result = await controller.Delete(id) as RedirectToRouteResult;

            //Assert
            newsServiceMock.Verify();
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", (string)result.RouteValues["action"]);
            Assert.AreEqual("News", (string)result.RouteValues["controller"]);
            Assert.AreEqual("AdminPanel", (string)result.RouteValues["area"]);
        }

        [TestMethod]
        [ExpectedException(typeof(ItemEditException))]
        public async Task Delete_ItemExistsDeleteError_ReturnHttpNotFound()
        {
            //Arrange
            int id = 1;

            var newsServiceMock = new Mock<INewsService>();
            var categoryServiceMock = new Mock<IService<CategoryDto>>();

            newsServiceMock.Setup(n => n.Delete(It.IsNotNull<NewsItemDto>())).ThrowsAsync(new ItemEditException(""));
            newsServiceMock.Setup(n => n.GetItem(id)).ReturnsAsync(new NewsItemDto());

            //Act
            var controller = new NewsController(newsServiceMock.Object, categoryServiceMock.Object, _mapper);
            await controller.Delete(id);
        }
    }
}
